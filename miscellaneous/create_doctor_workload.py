import pandas as pd
import json 

doctor_list = pd.read_csv('list_of_doctors.csv', sep=';', header=None)
doctor_list = doctor_list[0].tolist()

list_of_last_names = []
for doctor in doctor_list:
	list_of_last_names.append(doctor.split(',')[0].strip())

doctor_workload = {}

for name in list_of_last_names:
	doctor_workload[name.lower().strip()] = 0

with open('doctor_workload.json', 'w') as f:
	json.dump(doctor_workload, f)
