import pickle
import numpy as np

# load linear regression model
pickle_in = open('dataset/LinearRegressionM1.pickle','rb')
linear_regression_model = pickle.load(pickle_in)

def process_weights(vector, doctor_list):
    """
    Takes into input the vector for the row and the list of doctor full names

    Returns dictionary of { doctor_name : weight }
    The higher the weight, the more important the doctor is to making the team more efficient
    """

    number_of_doctors = len(doctor_list)

    doctor_vector = vector[:number_of_doctors]  # vector of doctors
    date_vector = vector[number_of_doctors:]    # vector of hour and date
    overall_number_of_doctors_in_selection = len([i for i, x in enumerate(doctor_vector) if x == 1])

    # overall PPH score
    overall_score = linear_regression_model.predict(np.array([vector]))[0]

    # dictionary with the doctor name key associated to the weight value
    doctor_weights = {}

    # get doctor indices
    doctor_indices = [i for i, x in enumerate(doctor_vector) if x == 1]

    for doctor_index in doctor_indices:

        # get doctor's full name
        doctor_full_name = doctor_list[doctor_index]

        # remove the specified doctor from the list (1 becomes 0)
        temp_doctor_vector = doctor_vector.copy()
        temp_doctor_vector[doctor_index] = 0
        combined_vector = temp_doctor_vector + date_vector

        # compute weight
        prediction = linear_regression_model.predict(np.array([combined_vector]))[0]

        print(overall_score)
        print(prediction)

        number_of_doctors_in_selection = len([i for i, x in enumerate(temp_doctor_vector) if x == 1])
        weight = overall_score/overall_number_of_doctors_in_selection - prediction/number_of_doctors_in_selection
        print(number_of_doctors_in_selection)

        # assign key and value to dictionary
        doctor_weights[doctor_full_name] = weight

    return doctor_weights

if __name__ == '__main__':
    input_vector = [0]*127
    input_vector[1] = 1
    input_vector[5] = 1
    input_vector[15] = 1
    input_vector[16] = 1
    input_vector[17] = 1
    input_vector[29] = 1
    input_vector[30] = 1
    input_vector[37] = 1
    input_vector[100] = 1
    input_vector[125] = 1
    doctor_names = [str(x) for x in range(91)]
    print(process_weights(input_vector, doctor_names))
