#Create Shift Restrictions
import pandas as pd
import json

data = {
    "Admin on call": [],

    "Backup day": [],

    "Backup night": [],

    "West day 7-4": [],

    "West day 7-5": [],

    "West day 9-4": [],

    "East day 7-5": [],

    "East day 9-4": [],

    "Hub day 9-5": [],

    "Hub day 10-6": [],

    "ST day 12-8": [],

    "ST eve 8-2": [],

    "West eve 3-12": [],

    "East eve 4-12": [],

    "Hub eve 5-1": [],

    "Hub eve 6-2": [],

    "Surge 8-2": [],

    "Surge 2-10": [],

    "Surge 6-12": [],

    "Surge 8-4": [],

    "Surge 4-10": [],

    "Surge 10-4": [],

    "Surge 10-6": [],

    "West eve 4-12": [],

    "West night": [],

    "X night 8-4": [],

    "X night 9-3": [],

    "X night 9-5": [],

    "EW 10-5 float": [] 
}

Schedule = pd.read_excel('Raw_Schedule/ByteBloc_Schedule_Shifts_Jan_9_Oct_22_2018-08-20.xlsx', header=6)
Schedule.drop(columns='Dates', inplace=True)
Shifts = Schedule.columns.values

tracker = 0

for index, row in Schedule.iterrows():
    if 0 < tracker < 651:
        tomorrow = Schedule.ix[index - 1]
        for shift, doctor in enumerate(row):
            restrictions_list = data[Shifts[shift]]
            for shift_, doctor_ in enumerate(tomorrow):
                if doctor == doctor_ and doctor!= '---' and Shifts[shift_] not in restrictions_list:
                    restrictions_list.append(Shifts[shift_])
    tracker += 1            

with open('shift_restrictions_tomorrow.json', 'w') as f:
	json.dump(data, f)

print(data)	
