#OBSOLETE
def get_all_doctors(schedule_table):
	all_available_doctors = []
	all_doctor_rows = []
	pph_scores = []
	time_vec = []
	for index, row in schedule_table.iterrows():
		#doctor_vector = [0] * len(list_of_doctors_total_csv)
		time_vector = [0] * 24
		time = (index + 7) % 24
		time_vector[time] = 1
		time_vec.append(time_vector)

		pph_scores.append(row['PPH score'])
		doctor_row = row.drop(labels=['Time', 'PPH score', 'Overall score']).tolist()
		all_doctor_rows.append(doctor_row)
		for name in doctor_row:
			if name != '':
				all_available_doctors.append(name)
		#all_available_doctors = vectorize_doctors(all_available_doctors, list_of_doctors_total_csv)
	all_available_doctors = sorted(list(set(all_available_doctors)))
	return all_available_doctors, all_doctor_rows, pph_scores, time_vec

#OBSOLETE
def list_replace(list_, first_element, second_element):
	new_list = []
	for i in list_:
		if i == first_element:
			new_list.append(second_element)
		else: new_list.append(i)
	return new_list		

#OBSOLETE
def get_new_pph(doctors_per_row, time_vec, doc, doctor_name, month_vector, model, list_of_doctors_total_csv):
	new_pph_scores = []
	new_doctors_per_row = []
	temp_df = pd.DataFrame(doctors_per_row)
	for column in temp_df:
		listed_column = temp_df[column].tolist()
		if doc in listed_column and doctor_name not in listed_column:
			listed_column = list_replace(listed_column, doc, doctor_name)
		elif doc not in listed_column and doctor_name in listed_column:
			listed_column = list_replace(listed_column, doctor_name, doc)	
		temp_df[column] = listed_column	

	for index, row in temp_df.iterrows():
		new_doctors_per_row.append(row.tolist())
		current_time_vec = [0] * 24
		time_index = (index + 7) % 24
		current_time_vec[time_index] = 1
		vectorized_vec = vectorize_doctors(row, list_of_doctors_total_csv)
		vectorized_vec = vectorized_vec + current_time_vec + month_vector
		new_pph_scores.append(model.predict(np.array([vectorized_vec])))
	new_pph_scores = np.array(new_pph_scores).flatten()
	new_pph_scores = [round(value, 1) for value in new_pph_scores]
	
	return new_pph_scores, new_doctors_per_row

#OBSOLETE
def get_new_table(table, new_pph_scores, new_rows):
	time = table['Time'].tolist()
	table.drop(columns=['Time', 'PPH score'], inplace=True)
	for index, row in table.iterrows():
		new_row = new_rows[index]
		table.ix[index] = new_row
	table['PPH score'] = new_pph_scores
	table.insert(loc=0, column='Time', value=time)
	return table

#OBSOLETE AND USELESS
def get_doctor_stats(new_pph_scores):
	total_patients_seen = sum(new_pph_scores)
	return total_patients_seen

#OBSOLETE
def get_thresholds(table, all_thresholds):
	pph_scores = table['PPH score'].tolist()
	new_table = table.drop(columns=['Time', 'PPH score'])
	column_names = list(new_table.columns.values)
	thresholds = []
	for column in column_names:
		thresholds.append(all_thresholds[column])

	pphpd_list = []
	thresholds_list = []
	for index, row in new_table.iterrows():
		list_of_thresholds_per_row = []
		for i, doctor in enumerate(row):
			if doctor != '':
				list_of_thresholds_per_row.append(thresholds[i])

		#Get averaged threshold for each row
		if len(list_of_thresholds_per_row) != 0:
			thresholds_list.append(sum(list_of_thresholds_per_row) / len(list_of_thresholds_per_row))
			pphpd_list.append(pph_scores[index] / len(list_of_thresholds_per_row))
		elif len(list_of_thresholds_per_row) == 0:
			thresholds_list.append(3.56)
			pphpd_list.append(2.67)	

	return thresholds_list, pphpd_list

#OBSOLETE
def compute_changes_in_day(local_available_docs, doctors_per_row, time_vec, doctor, month_vector, model, pph_scores, original_doctors_per_row, proposed_table, list_of_doctors_total_csv):
	solution_found = 0
	chosen_ind = []
	chosen_pph = []
	chosen_rows = []
	for doc in local_available_docs:
		new_pph_scores, new_doctors_per_row = get_new_pph(doctors_per_row, time_vec, doc, doctor, month_vector, model, list_of_doctors_total_csv)
		total_patients_seen = get_doctor_stats(new_pph_scores)

		#SET NEW THRESHOLD FOR IDENTIFYING GOOD REPLACEMENTS
		if total_patients_seen > pph_scores:
			solution_found = 1 
			chosen_ind.append(total_patients_seen)
			chosen_pph.append(new_pph_scores)
			chosen_rows.append(new_doctors_per_row)
			doctors_per_row = new_doctors_per_row
		else: doctors_per_row = original_doctors_per_row	

	if solution_found == 1:
		best = max(chosen_ind)
		best_index = chosen_ind.index(best)
		bestpph = chosen_pph[best_index]
		best_rows = chosen_rows[best_index]
		doctors_per_row = best_rows
		original_doctors_per_row = best_rows
		proposed_table = get_new_table(proposed_table, bestpph, best_rows)	
	else: 
		best = pph_scores
	
	return proposed_table, doctors_per_row, original_doctors_per_row, best

#OBSOLETE
def make_html_replacements(raw_table, proposed_table, original_table, all_thresholds, plus, original_patients_seen_per_day, day_change_check):
	calendar_colour = ''
	html_table = raw_table.to_html(index=False)
	original_string = """<table border="1" class="dataframe">"""
	new_string = """<table style="width:auto; table-layout:fixed;" class="table table-bordered">"""
	html_table = html_table.replace(original_string, new_string)
	html_table = html_table.replace("""!""", """<p>""")
	html_table = html_table.replace("""!!""", """</p>""")
	html_table = html_table.replace("""?""", """<p class=yellow>""")
	html_table = html_table.replace("""??""", """</p>""")
	html_table = html_table.replace("""<th>Time</th>""", """<th width=90>Time</th>""")
	html_table = html_table.replace("""<td>Green</td>""", """<td bgcolor="#19D529"></td>""")
	html_table = html_table.replace("""<td>Yellow</td>""", """<td bgcolor="#FFD400"></td>""")
	html_table = html_table.replace("""<td>Red</td>""", """<td bgcolor="#FF4900"></td>""")	
	html_table = html_table.replace("""<td>Orange</td>""", """<td bgcolor="#FFC300"></td>""")

	if proposed_table.equals(original_table):
		int_plus_minus = 0.0
		plus_minus = '+ 0.0'
		Overall = raw_table['Overall score'].tolist()
		if Overall.count('Red') >= 2:
			calendar_colour = '#FF4900'
		elif Overall.count('Yellow') >= 3:
			calendar_colour = '#FFD400'
		elif Overall.count('Orange') > 1:
			calendar_colour = '#FFC300'
		else: 
			calendar_colour = '#19D529'
		proposed_table['Overall score'] = Overall
		proposed_html_table = "<p align='left' style='margin-left: 80px;'>No Better Solution Was Found!</p><br>" + proposed_table.to_html(index=False)

	else:	
		thresholds, pphpd_list = get_thresholds(proposed_table, all_thresholds)
		int_plus_minus = int(round(plus - original_patients_seen_per_day))
		plus_minus = '+' + str(int(round(plus - original_patients_seen_per_day)))

		exp_thresholds_list = exponentiate_critic(thresholds)
		thresholds_list = exponentiate_medium(thresholds)
		exp_pphpd_list = exponentiate(pphpd_list)

		Overall = calculate_overall_score(exp_pphpd_list, thresholds_list, exp_thresholds_list, day_change_check)
		if Overall.count('Red') >= 2:
			calendar_colour = '#FF4900'
		elif Overall.count('Yellow') >= 3:
			calendar_colour = '#FFD400'
		elif Overall.count('Orange') > 1:
			calendar_colour = '#FFC300'
		else: 
			calendar_colour = '#19D529'

		proposed_table['Overall score'] = Overall

		proposed_html_table = proposed_table.to_html(index=False)

	proposed_html_table = proposed_html_table.replace(original_string, new_string)
	proposed_html_table = proposed_html_table.replace("""<th>Time</th>""", """<th width=100>Time</th>""")
	proposed_html_table = proposed_html_table.replace("""<td>Green</td>""", """<td bgcolor="#19D529"></td>""")
	proposed_html_table = proposed_html_table.replace("""<td>Yellow</td>""", """<td bgcolor="#FFD400"></td>""")
	proposed_html_table = proposed_html_table.replace("""<td>Red</td>""", """<td bgcolor="#FF4900"></td>""")
	proposed_html_table = proposed_html_table.replace("""<td>Orange</td>""", """<td bgcolor="#FFC300"></td>""")

	return html_table, proposed_table, proposed_html_table, plus_minus, int_plus_minus, calendar_colour

#OBSOLETE
def change_background_color(new_replacements, background_color):
	colors = ['#FF4900', '#FFD400', '#19D529', '#FFC300']
	for index, list_ in enumerate(new_replacements):
		length = len(list_)
		for i, string in enumerate(list_):
			new_color = background_color[i]
			for color in colors:
				if color in string:
					repl = new_replacements[index][i].replace(color, new_color)
					new_replacements[index][i] = repl
		background_color = background_color[length:]
		
	return new_replacements	

def optimize_BB_schedule(date, og_dict, permanent_shifts_PPH, list_of_last_names, backup_shifts, all_thresholds, weekends, night_shifts, day_shifts, shift_types, doctor_workload, doctor_shifts, list_of_doctors_total_csv, model, model1, time_index):
	#Month Vector
	month = date.split('-')[1]
	month_vector = [0] * 12
	month_vector[int(month) - 1] = 1

	master_list = [og_dict[x] for x in permanent_shifts_PPH]

	pph_sum = 0
	counter = 0
	objective = False
	while objective == False:
		shuffled_master_list = [random.sample(x, len(x)) for x in master_list]
		random_doctor_row = random_doctor_row_from_master_list(shuffled_master_list)
		day_schedule = pd.DataFrame(index=range(24))
		day_schedule = fill_day_schedule(permanent_shifts_PPH, random_doctor_row, list_of_last_names, doctor_shifts, list_of_doctors_total_csv, day_schedule)
		pph_list, pi_list = vectorize_row_with_PIP(date, day_schedule, month_vector, model, model1, list_of_doctors_total_csv)

		Overall = calculate_overall_score_with_PIP(pph_list, pi_list)
		pph_sum = sum(pph_list)
		pi_sum = sum(pi_list)

		#Check and Increment Counter
		counter += 1
		print(date, counter)

		if pph_sum < pi_sum:
			continue

		else: 
			for i, doc in enumerate(random_doctor_row):
				doctor_workload[doc] += (doctor_shifts[permanent_shifts_PPH[i]][1] - doctor_shifts[permanent_shifts_PPH[i]][0])
				update_restrictions(doc, permanent_shifts_PPH[i], date, backup_shifts, weekends, night_shifts, day_shifts, shift_types)

			day_schedule['Time'] = time_index
			day_schedule.set_index('Time', inplace=True)
			day_schedule['PPH score'] = pph_list
			day_schedule['Overall score'] = Overall
			day_schedule.reset_index(inplace=True)
			day_schedule.replace(np.nan, '', regex = True, inplace=True)

			objective = True

	return day_schedule, pph_sum, pi_sum

def update_list_of_available_doctors(date, previous_day_schedule, next_day_schedule, doctor_workload, available_shifts, available_doctors, shift_restrictions_y, shift_restrictions_t, doctor_shifts, day_shifts, night_shifts, weekends, backup_shifts, max_limit, max_backup_shifts, max_day_shifts, max_night_shifts, max_weekends, shift_types):
	current_weekday = datetime.datetime.strptime(date, "%Y-%m-%d").weekday()
	og_dict = create_new_og_dict(available_shifts)
	if previous_day_schedule is not None and next_day_schedule is not None:
		previous_day_docs = get_doctors_and_shifts(previous_day_schedule)
		next_day_docs = get_doctors_and_shifts(next_day_schedule)
		for shift in available_shifts:
			shift_type = shift_types[shift]
			for doctor in available_doctors:
				if doctor_workload[doctor] + (doctor_shifts[shift][1] - doctor_shifts[shift][0]) >= max_limit[doctor]: continue													#Check Doctor Workload
				if day_shifts[doctor] >= max_day_shifts[doctor] and shift_type == 'day': continue																				#Check Day Shifts
				if night_shifts[doctor] >= max_night_shifts[doctor] and shift_type == 'night': continue																			#Check Night Shifts
				if backup_shifts[doctor] >= max_backup_shifts[doctor] and shift_type == 'backup': continue																		#Check Backup Shifts
				if weekends[doctor] >= max_weekends[doctor] and current_weekday == 5 or weekends[doctor] >= max_weekends[doctor] and current_weekday == 6: continue				#Check Weekend Shifts

				elif doctor not in previous_day_docs.values() and doctor not in next_day_docs.values() and doctor not in og_dict[shift]:
					og_dict[shift].append(doctor)

				elif doctor in previous_day_docs.values() and doctor not in next_day_docs.values() and doctor not in og_dict[shift]:	
					y_shift = [key for key, value in previous_day_docs.items() if value == doctor][0]
					if shift in shift_restrictions_y[y_shift]:
						og_dict[shift].append(doctor)

				elif doctor not in previous_day_docs.values() and doctor in next_day_docs.values() and doctor not in og_dict[shift]:	
					t_shift = [key for key, value in next_day_docs.items() if value == doctor][0]
					if shift in shift_restrictions_t[t_shift]:
						og_dict[shift].append(doctor)

				elif doctor in previous_day_docs.values() and doctor in next_day_docs.values() and doctor not in og_dict[shift]:
					y_shift = [key for key, value in previous_day_docs.items() if value == doctor][0]
					t_shift = [key for key, value in next_day_docs.items() if value == doctor][0]
					if shift in shift_restrictions_y[y_shift] and shift in shift_restrictions_t[t_shift]:
						og_dict[shift].append(doctor)

	elif previous_day_schedule is not None and next_day_schedule is None:
		previous_day_docs = get_doctors_and_shifts(previous_day_schedule)
		for shift in available_shifts:
			shift_type = shift_types[shift]
			for doctor in available_doctors:
				if doctor_workload[doctor] + (doctor_shifts[shift][1] - doctor_shifts[shift][0]) >= max_limit[doctor]: continue													#Check Doctor Workload
				if day_shifts[doctor] >= max_day_shifts[doctor] and shift_type == 'day': continue																				#Check Day Shifts
				if night_shifts[doctor] >= max_night_shifts[doctor] and shift_type == 'night': continue																			#Check Night Shifts
				if backup_shifts[doctor] >= max_backup_shifts[doctor] and shift_type == 'backup': continue																		#Check Backup Shifts
				if weekends[doctor] >= max_weekends[doctor] and current_weekday == 5 or weekends[doctor] >= max_weekends[doctor] and current_weekday == 6: continue				#Check Weekend Shifts

				elif doctor not in previous_day_docs.values() and doctor not in og_dict[shift]: og_dict[shift].append(doctor)
				elif doctor in previous_day_docs.values() and doctor not in og_dict[shift]:
					y_shift = [key for key, value in previous_day_docs.items() if value == doctor][0]
					if shift in shift_restrictions_y[y_shift]:
						og_dict[shift].append(doctor)

	elif previous_day_schedule is None and next_day_schedule is not None:
		next_day_docs = get_doctors_and_shifts(next_day_schedule)
		for shift in available_shifts:
			shift_type = shift_types[shift]
			for doctor in available_doctors:
				if doctor_workload[doctor] + (doctor_shifts[shift][1] - doctor_shifts[shift][0]) >= max_limit[doctor]: continue													#Check Doctor Workload
				if day_shifts[doctor] >= max_day_shifts[doctor] and shift_type == 'day': continue																				#Check Day Shifts
				if night_shifts[doctor] >= max_night_shifts[doctor] and shift_type == 'night': continue																			#Check Night Shifts
				if backup_shifts[doctor] >= max_backup_shifts[doctor] and shift_type == 'backup': continue																		#Check Backup Shifts
				if weekends[doctor] >= max_weekends[doctor] and current_weekday == 5 or weekends[doctor] >= max_weekends[doctor] and current_weekday == 6: continue				#Check Weekend Shifts

				elif doctor not in next_day_docs.values() and doctor not in og_dict[shift]: og_dict[shift].append(doctor)
				elif doctor in next_day_docs.values() and doctor not in og_dict[shift]:
					t_shift = [key for key, value in next_day_docs.items() if value == doctor][0]
					if shift in shift_restrictions_t[t_shift]:
						og_dict[shift].append(doctor)
						
	else:
		for shift in available_shifts:
			shift_type = shift_types[shift]
			for doctor in available_doctors:
				if doctor_workload[doctor] + (doctor_shifts[shift][1] - doctor_shifts[shift][0]) >= max_limit[doctor]: continue													#Check Doctor Workload
				if day_shifts[doctor] >= max_day_shifts[doctor] and shift_type == 'day': continue																				#Check Day Shifts
				if night_shifts[doctor] >= max_night_shifts[doctor] and shift_type == 'night': continue																			#Check Night Shifts
				if backup_shifts[doctor] >= max_backup_shifts[doctor] and shift_type == 'backup': continue																		#Check Backup Shifts
				if weekends[doctor] >= max_weekends[doctor] and current_weekday == 5 or weekends[doctor] >= max_weekends[doctor] and current_weekday == 6: continue				#Check Weekend Shifts
				
				elif doctor not in og_dict[shift]: og_dict[shift].append(doctor)

	return og_dict

def get_day_schedule(schedule_col_names, temp_row, list_of_last_names, doctor_shifts, all_thresholds, list_of_doctors_total_csv, day_schedule):
	thresholds = []
	for column, value in zip(schedule_col_names, temp_row):
		for ind, lastname in enumerate(list_of_last_names):
			if value.lower() == lastname.lower():
				list_range_hours = doctor_shifts[column]

				thresholds.append(all_thresholds[column])

				start = list_range_hours[0] - 7
				end = list_range_hours[1] - 7

				doctor_full_name_current = list_of_doctors_total_csv[ind]

				for hour in range(start, end):
					day_schedule = day_schedule.set_value(hour, column, doctor_full_name_current)   

	return thresholds, day_schedule

def calculate_overall_score(exp_pphpd_list, thresholds_list, exp_thresholds_list, day_change_check):
	Overall = []
	for pphpd, threshold, critic_threshold, dcheck in zip(exp_pphpd_list, thresholds_list, exp_thresholds_list, day_change_check):
		if critic_threshold == 2.67 and pphpd == 2.67:
			Overall.append('No Doctor Registered')
		elif pphpd >= threshold:
			Overall.append('Green')
		elif dcheck == 1 and pphpd < threshold:
			Overall.append('Orange')
		elif pphpd < threshold and pphpd >= critic_threshold:
			Overall.append('Yellow')
		else: Overall.append('Red')		
	return Overall

def exponentiate_critic(values):
	new_values = []
	for value in values:
		a = ((0.75 * value) - 2.67)
		if a >= 0:
			new_value = (a ** 2) + 2.67
			new_values.append(new_value)
		elif a < 0:
			new_value = -(a ** 2) + 2.67
			new_values.append(new_value)
	return new_values

def exponentiate_medium(values):
	new_values = []
	for value in values:
		a = ((0.85 * value) - 2.67)
		if a >= 0:
			new_value = (a ** 2) + 2.67
			new_values.append(new_value)
		elif a < 0:
			new_value = -(a ** 2) + 2.67
			new_values.append(new_value)
	return new_values		

def exponentiate(values):
	new_values = []
	for value in values:
		a = (value - 2.67)
		if a >= 0:
			new_value = (a ** 2) + 2.67
			new_values.append(new_value)
		elif a < 0:
			new_value = -(a ** 2) + 2.67
			new_values.append(new_value)
	return new_values	

def vectorize_row(date, day_schedule, month_vec, model, model1, thresholds, list_of_doctors_total_csv):
	pph_list = []
	pi_list = []
	pphpd_list = []
	thresholds_list = []
	day_change_check = []
	weekday_vec = weekto(datetime.datetime.strptime(date, "%Y-%m-%d").weekday() - 1)
	for index, row in day_schedule.iterrows():
		list_of_thresholds_per_row = []
		list_of_doctors_per_row = []
		for i, doctor in enumerate(row):
			if pd.isnull(doctor) == False:
				list_of_thresholds_per_row.append(thresholds[i])
				list_of_doctors_per_row.append(doctor)

		vectorized_row_doctors = vectorize_doctors(list_of_doctors_per_row, list_of_doctors_total_csv)
		if vectorized_row_doctors.count(1) == 1:
			day_change_check.append(1)
		else: day_change_check.append(0)	
		vectorized_hour = timeto((index + 7) % 24)

		vector = vectorized_row_doctors + vectorized_hour + month_vec
		vector1 = vectorized_hour + month_vec + weekday_vec
		prediction = model.predict(np.array([vector]))
		prediction1 = model1.predict(np.array([vector1]))

		#Get averaged threshold for each row
		if len(list_of_thresholds_per_row) != 0:
			thresholds_list.append(sum(list_of_thresholds_per_row) / len(list_of_thresholds_per_row))
			pphpd_list.append(prediction / len(list_of_doctors_per_row))
			pph_list.append(prediction)
			pi_list.append(prediction1)
		elif len(list_of_thresholds_per_row) == 0:
			pphpd_list.append(2.67)  
			pph_list.append([0.0])
			pi_list.append(prediction1)
			thresholds_list.append(3.56)

	pph_list = np.array(pph_list).flatten()
	pph_list = [round(value, 1) for value in pph_list]
	pi_list = np.array(pi_list).flatten()
	pi_list = [round(value, 1) for value in pi_list]
	pphpd_list = np.array(pphpd_list).flatten()
			
	return 	pph_list, pi_list, pphpd_list, thresholds_list, day_change_check

def optimize_schedule_by_swapping_with_overstaffed_days(schedule_tables_dict, list_days_index_pph_minus_pi_ascending_order, 
	predict_pph_value_model, predict_pi_value_model, list_of_doctors_total_csv, all_calendar_days, list_of_secondary_shift_names):
	"""Alternative method to optimize schedule
	PPH: 'model'
	PI: 'model1'
	"""

	# start with days with overstaffing
	for day_index in reverse(list_days_index_pph_minus_pi_ascending_order):

		# string of current date
		date = all_calendar_days[day_index]

		# current_day_table = schedule_tables_dict[day_index]
		month = date.split('-')[1]
		month_vector = [0] * 12
		month_vector[int(month) - 1] = 1

		day_schedule = schedule_tables_dict[day_index]

		pph_list, pi_list = vectorize_row_with_PIP(date, day_schedule, month_vec, model=predict_pph_value_model, model1=predict_pi_value_model, list_of_doctors_total_csv=list_of_doctors_total_csv)
		pph_sum = sum(pph_list)
		pi_sum = sum(pi_list)

		copy_of_list_of_secondary_shift_names = list_of_secondary_shift_names

		# check if pph value is higher and pi value (overstaffing):
		max_counter = 0		# safety measure to prevent infinite loop
		list_of_doctors_removed_for_current_day = []

		while (pph_sum - pi_sum) > 0 and max_counter < 100:
			list_of_current_shift_names = list(day_schedule.columns.values)

			# if there is overstaffing, take out a shift and compute PPH - PI value
			shift_to_remove = random.choice(list_of_secondary_shift_names)
			if shift_to_remove not in list_of_current_shift_names:
				list_of_doctors_removed_for_current_day.append(day_schedule[shift_to_remove].values)
				day_schedule = day_schedule.drop(columns=[shift_to_remove])

			# computing PPH - PI value
			pph_list, pi_list = vectorize_row_with_PIP(date, day_schedule, month_vec, model=predict_pph_value_model, model1=predict_pi_value_model, list_of_doctors_total_csv=list_of_doctors_total_csv)
			pph_sum = sum(pph_list)
			pi_sum = sum(pi_list)

		print(list_of_doctors_removed_for_current_day)