"""
Computing the waiting time for each doctor
"""

import pandas as pd 


df = pd.read_excel("dataset/Copy of Sample Data(172).xlsx", sheet_name="Sheet1")

list_of_unique_doctor_names = df["StaffMD"].unique().tolist()
print(list_of_unique_doctor_names)

dict_of_doctors__waiting_time_list = {doctor_name:[] for doctor_name in list_of_unique_doctor_names}

for index, row in df.iterrows():
	

	NPIA_string = row["PIA_NPIA"][:-3]
	Arrival_string = row["Arrival"][:-3]

	d1 = datetime.datetime.strptime(NPIA_string, '-%m-%Y:%H:%M:%S')
	d2 = datetime.datetime.strptime(Arrival_string, '%Y:%m:%d:%H:%M:%S')
	diff = (d2 - d1).total_seconds() / 60		# difference in minutes
