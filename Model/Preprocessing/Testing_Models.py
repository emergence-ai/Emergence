import pandas as pd
import numpy as np
import string
import os 
import csv
import sys
from io import StringIO

import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.utils import to_categorical
from keras.callbacks import EarlyStopping, ModelCheckpoint
from keras import regularizers
from sklearn.metrics import mean_squared_error, log_loss

from sklearn.linear_model import LinearRegression, SGDClassifier
from sklearn.decomposition import PCA
from sklearn.model_selection import learning_curve
from sklearn.model_selection import ShuffleSplit

import matplotlib.pyplot as plt

from numpy import genfromtxt


X = pd.read_csv('X_train_29-10-18.csv', sep=',',header=None)
y = genfromtxt('y_train_29-10-18.csv', delimiter=',')

# Build model

def binary_network(X, y):
    """
    Neural network used for binary classification of PPH threshold of 8
    :param: X: training data input
    :param: y: labels
    """
    model = Sequential()
    model.add(Dense(20, input_dim=X.shape[1], activation="relu", kernel_initializer="normal"))
    model.add(Dropout(0.8))
    model.add(Dense(10, activation="relu", kernel_initializer="normal"))
    model.add(Dropout(0.5))
    model.add(Dense(1, activation="sigmoid", kernel_initializer="normal"))
    model.compile(loss="mean_squared_error", optimizer="Adam",metrics=['acc'])
    return model

def wide_regression_network(X, y):
    """
    Wide neural network used for regression with PPH
    :param: X: training data input
    :param: y: labels
    """
    model = Sequential()
    model.add(Dense(256, input_dim=X.shape[1], activation="relu"))
    model.add(Dropout(0.8))
    model.add(Dense(20, activation="relu"))
    model.add(Dropout(0.2))
    model.add(Dense(1, activation="relu"))
    model.compile(loss="mean_squared_error", optimizer="adam",metrics=['accuracy'])
    return model 

def plot_graphs(history):
    """
    Function used to plot accuracy and loss of model
    :param: history: from Sequential()
    """
    # summarize history for accuracy
    plt.plot(history.history['acc'])
    plt.plot(history.history['val_acc'])
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.savefig("acc.png")
    plt.close()
    # summarize history for loss
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.savefig("loss.png")

    plt.show()

def complex_sequential(X, y):
    # Build model
    model = Sequential()
    model.add(Dense(256, input_dim=X.shape[1], activation="relu"))
    model.add(Dropout(0.8))
    # model.add(Dense(128, activation="relu"))
    # model.add(Dropout(0.5))
    #model.add(Dense(64, activation="relu"))
    # model.add(Dropout(0.3))
    model.add(Dense(20, activation="relu"))
    model.add(Dropout(0.2))
    model.add(Dense(1, activation="relu"))
    # adam = keras.optimizers.Adam(lr=0.01, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False)
    model.compile(loss="mean_squared_error", optimizer="adam", metrics=['accuracy'])
    return model

'''
# build model
model = complex_sequential(X, y)

# Run model
earlystopper = EarlyStopping(patience=100, verbose=1)
checkpointer = ModelCheckpoint('ER-Efficiency-model3.h5', verbose=1, save_best_only=True)
history = model.fit(X, y, epochs= 50, batch_size=64, validation_split=0.2, callbacks=[earlystopper, checkpointer])


plot_graphs(history)
'''

clf = LinearRegression(n_jobs=-1)
clf.fit(X, y)
clf.score(X, y)

predictions = clf.predict(X)
mse = mean_squared_error(y, predictions)
#loss = log_loss(y, predictions)

print(mse)


