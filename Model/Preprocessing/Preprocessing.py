import pandas as pd
import numpy as np
import string
import os 
import datetime


# Read Dataset
path = os.listdir('/Users/DevSK/Documents/Emergence/Data/EDIS_rawdata_2017/')

# PATHNAME = ['/Users/DevSK/Documents/Emergence/Data/EDIS_rawdata_2017/Aug.xls', '/Users/DevSK/Documents/Emergence/Data/EDIS_rawdata_2017/Apr.xls']

# pandas dataframe of all doctor inputs used for training (doctor names)
df_data = pd.read_excel('/Users/DevSK/Documents/Emergence/Data/EDIS_rawdata_2017/' + path[0]).dropna()

for index in range(len(path) - 1):
    new_df = pd.read_excel('/Users/DevSK/Documents/Emergence/Data/EDIS_rawdata_2017/' + path[index + 1]).dropna()
    df_data = pd.concat([new_df, df_data], ignore_index=True)


X = []      # training dataset  [hour and month]
y = []      # PPH time  [average PPH]

# Process data
processed_dataset = []
for row in range(df_data.shape[0]):
    processed_temp_row = []
    processed_temp_row.append(df_data['Arrival'][row])
    processed_temp_row.append(df_data["PIA_NPIA"][row])
    processed_dataset.append(processed_temp_row)

date_codes = []
for row in processed_dataset:
    day = str(row[1].day)
    if len(str(row[1].day)) == 1:
        day = '0' + str(row[1].day)

    # ###################### 1 HOUR BLOCK ########################################
    # number_of_hours_per_block = 1   ### 1 HOUR (COMMENT!) TODO CHANGE THIS VARIABLE (NUMBER OF HOURS PER BLOCK)

    number_of_hours_per_block = 1    # TODO CHANGE THIS VARIABLE (NUMBER OF HOURS PER BLOCK)
    current_date = str(row[1].month) + '-' + day + '-' + str(row[1].year) + ' ' + str(int(row[1].hour / number_of_hours_per_block))
    date_codes.append(current_date)

date_codes = np.array(date_codes)
df_data['date_codes'] = date_codes
date_codes_unique = np.sort(np.unique(date_codes))            #Getting unique date/hour to group rows per hour block

#for unique date codes
for date_code in date_codes_unique: 
    df_selection = df_data.ix[df_data["date_codes"] == date_code]         #Grouping rows per unique date/hour block_hours					   

    #Define date
    only_date = date_code.split(' ')
    month, day, year = int(only_date.split('-').strip())

    # initialize hour vector
    hour = int(date_code.split(' ')[1].strip())
    hour_vector = [0] * 24
    hour_vector[hour] = 1

    # initialize vectors for the month
    month_vec = [0] * 12
    month_vec[month - 1] = 1

    #Initialize vector for weekday
    weekday_vec = [0] * 7
    weekday = datetime.datetime(year, month, day).weekday()
    weekday_vec[weekday - 1] = 1

    # Add vectors to training data
    X.append(hour_vector + month_vec + weekday_vec)
    tp = float(len(df_selection))                                      #Defining patients per hour (PPH) by taking the number of rows. 
    y.append(tp)


# convert lists to NumPy array
X = np.array(X)
y = np.array(y)

# save training and testing data as CSV files
np.savetxt("X_train_29-10-18.csv", X, delimiter=",")
np.savetxt("y_train_29-10-18.csv", y, delimiter=',')
