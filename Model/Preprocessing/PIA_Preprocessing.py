import pandas as pd
import numpy as np
import string

from keras.models import Sequential
from keras.layers import Dense
from keras.utils import to_categorical

from keras.callbacks import EarlyStopping, ModelCheckpoint

def PIACONV(start, end):
    """Function that converts 2 strings into PIA time (int) seconds and assigns score."""
    hours = int((start[11:13])) * 3600
    minutes = int((start[14:16])) * 60

    time1 = (hours + minutes)

    hours2 = int((end[11:13])) * 3600
    minutes2 = int((end[14:16])) * 60

    time2 = (hours2 + minutes2)

    if (time2 > time1):
        PIATIME = (time2-time1)
    else:
        PIATIME = (time2 + (86400 - time1))

    if (PIATIME < 7200):
        lvl = 3
    elif (PIATIME > 7200 and PIATIME < 14400):
        lvl = 2
    elif (PIATIME > 14400):
        lvl = 1
    return lvl


# Read Dataset from multiple excel sheets and concatenating
PATHNAME = ['/Users/DevSK/Documents/Emergence/Data/EDIS_rawdata_2017/Apr.xls']
df_data = pd.read_excel(PATHNAME[0])

for index in range(len(PATHNAME) - 1):
    new_df = pd.read_excel(PATHNAME[index+1])
    df_data = pd.concat([new_df, df_data])

processed_dataset = []

start_list = df_data['Arrival'].tolist()
end_list = df_data["PIA_NPIA"].tolist()

X = []      # training dataset  [vector of doctors per 4-hour block]
y = []      # PIA time  [average PIA time per 4-hour block]

# Process data
for row in range(df_data.shape[0]):
    processed_temp_row = []
    processed_temp_row.append(df_data['Arrival'][row])
    processed_temp_row.append(df_data["PIA_NPIA"][row])
    processed_temp_row.append(df_data["StaffMD"][row])
    processed_dataset.append(processed_temp_row)

#List of All doctors in dataset
list_of_doctors = list(df_data["StaffMD"].unique())

doctors_present = []    # list of doctors present
block_hours = []        # list of PIA times per hour block
doctors_vector = [0] * len(list_of_doctors)

date_codes = []
for row in processed_dataset:
    day = str(row[0].day)              #Getting day from Arrival Date
    if len(str(row[0].day)) == 1:      #Adding 0 if date < 10
        day = '0' + str(row[0].day)

    current_date = str(row[0].month)+'-'+day+'-'+str(row[0].year)+' '+str((row[0].hour) % 6)  #Getting stringed date and hour from Arrival Date
    date_codes.append(current_date)                                                           

date_codes = np.array(date_codes)

df_data['date_codes'] = date_codes
date_codes_unique = np.sort(np.unique(date_codes))                                            #Getting unique date/hour to group rows per hour block

# for unique date codes
for date_code in date_codes_unique:
    # select unique date codes
    df_selection = df_data.ix[df_data["date_codes"] == date_code]                             #Grouping rows per unique date/hour blocks
    block_hours = []
    doctors_vector = [0]*len(list_of_doctors)

    for df_row in df_selection.iterrows():
        index, row = df_row

        # compute doctor vector in one hot encode
        for doctor_index in range(len(list_of_doctors)):
            if list_of_doctors[doctor_index] == row["StaffMD"]:
                doctors_vector[doctor_index] = 1  

        # compute PIA time                                              
        block_hours.append(PIACONV(str(row["Arrival"]), str(row["PIA_NPIA"])))           #Getting the scored version of PIA time based on PIACONV function

    X.append(doctors_vector)
    
    tscore = sum(block_hours)
    y.append(tscore)


X = np.array(X)
y = np.array(y)

y = to_categorical(y)

# Build model
model = Sequential()
model.add(Dense(50, input_dim=X.shape[1], activation="relu"))
model.add(Dense(40, activation="relu"))
model.add(Dense(20, activation="relu"))
model.add(Dense(3, activation="relu"))
model.compile(loss="mean_squared_error", optimizer="adam",metrics=['accuracy'])

# Run model
earlystopper = EarlyStopping(patience=5, verbose=1)
checkpointer = ModelCheckpoint('ER-Efficiency-model1.h5', verbose=1, save_best_only=True)
history = model.fit(X, y, epochs= 75, batch_size=32, validation_split=0.2, callbacks=[earlystopper, checkpointer])

#model.save("trained_model_to_reduce_PIA.h5")


print (y)
