import pandas as pd
import numpy as np
import string
import os 


# Read Dataset
path = os.listdir('/Users/DevSK/Documents/Emergence/Data/EDIS_rawdata_2017/')

# PATHNAME = ['/Users/DevSK/Documents/Emergence/Data/EDIS_rawdata_2017/Aug.xls', '/Users/DevSK/Documents/Emergence/Data/EDIS_rawdata_2017/Apr.xls']

# pandas dataframe of all doctor inputs used for training (doctor names)
df_data = pd.read_excel('/Users/DevSK/Documents/Emergence/Data/EDIS_rawdata_2017/' + path[0]).dropna()

for index in range(len(path) - 1):
    new_df = pd.read_excel('/Users/DevSK/Documents/Emergence/Data/EDIS_rawdata_2017/' + path[index + 1]).dropna()
    df_data = pd.concat([new_df, df_data], ignore_index=True)

#Get Unique List Of Doctors In Dataset

list_of_doctors = list(df_data["StaffMD"].unique())
np.savetxt("list_of_doctors_v2.csv", list_of_doctors, delimiter=';', fmt='%s')


X = []      # training dataset  [vector of doctors per 1-hour block]
y = []      # PPH time  [average PPH]

# Process data
processed_dataset = []
for row in range(df_data.shape[0]):
    processed_temp_row = []
    processed_temp_row.append(df_data['Arrival'][row])
    processed_temp_row.append(df_data["PIA_NPIA"][row])
    processed_temp_row.append(df_data["StaffMD"][row])
    processed_dataset.append(processed_temp_row)

doctors_present = []                       # list of doctors present      
doctors_vector = [0]*len(list_of_doctors)
average_PIA = 0.0

date_codes = []
hour_codes = []
month = [] 

for row in processed_dataset:
    day = str(row[1].day)
    if len(str(row[1].day)) == 1:
        day = '0' + str(row[1].day)

    # ###################### 1 HOUR BLOCK ########################################
    # number_of_hours_per_block = 1   ### 1 HOUR (COMMENT!) TODO CHAMGE THIS VARIABLE (NUMBER OF HOURS PER BLOCK)

    number_of_hours_per_block = 1    # TODO CHAMGE THIS VARIABLE (NUMBER OF HOURS PER BLOCK)
    current_date = str(row[1].month)+'-'+day+'-'+str(row[1].year)+' '+str(int(row[1].hour/number_of_hours_per_block))
    date_codes.append(current_date)
    hour_codes.append(int(row[1].hour/number_of_hours_per_block))

date_codes = np.array(date_codes)
hour_codes = np.array(hour_codes)

df_data['date_codes'] = date_codes
df_data['hour_codes'] = hour_codes

date_codes_unique = np.sort(np.unique(date_codes))            #Getting unique date/hour to group rows per hour block

#Patients per hour per doctor = PPH / number of doctors
listed_PPHPD = []

# for unique date codes
for date_code in date_codes_unique: 
    df_selection = df_data.ix[df_data["date_codes"] == date_code]         #Grouping rows per unique date/hour block_hours

    doctors_vector = [0] * len(list_of_doctors)						      #Defining one hot vector of doctors

    for df_row in df_selection.iterrows():
        index, row = df_row

        # compute doctor vector
        for doctor_index in range(len(list_of_doctors)):
            if list_of_doctors[doctor_index] == row["StaffMD"]:
                doctors_vector[doctor_index] = 1    					  #Setting active doctors to 1

        hour = row["hour_codes"]

    # initialize hour vector
    hour_vector = [0]*24
    hour_vector[hour] = 1

    # initialize vectors for the month
    month_vec = [0]*12
    month_int = int(date_code.split('-')[0])
    month_vec[month_int-1] = 1

    # Add vectors to training data
    X.append(doctors_vector+hour_vector+month_vec)
    
    tp=(float(len(df_selection)))                                        #Defining patients per hour (PPH) by taking the number of rows. 
    PPHPD = tp / doctors_vector.count(1)
    listed_PPHPD.append(PPHPD)

    y.append(tp)

#Exponentiating Target Values
data_average_and_median = 2.67
exp_values = []
for value in listed_PPHPD:
    new_value = value - data_average_and_median
    if new_value >= 0:
        exp_value = (new_value ** 2) + data_average_and_median
        exp_values.append(exp_value)
    elif new_value < 0:
        exp_value = -(new_value ** 2) + data_average_and_median
        exp_values.append(exp_value)


# convert lists to NumPy array
X = np.array(X)
y = np.array(y)


# save training and testing data as CSV files

np.savetxt("X_train_22-08-18.csv", X, delimiter=",")
np.savetxt("y_train_22-08-18.csv", y, delimiter=',')


#Visualize some data
print(np.max(y.flatten()))
print(np.min(y.flatten()))
print(np.mean(y.flatten()))
print(np.std(y.flatten()))