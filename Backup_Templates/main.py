from flask import Flask, redirect, url_for, render_template, request
from flask_uploads import UploadSet, configure_uploads, IMAGES
import pandas as pd

import glob
import os

#from keras.models import load_model

import collections
import numpy as np
import pandas as pd
import string
import os
import csv
import json
import datetime
import math
import pickle
import operator
import calendar

pd.options.display.max_columns = 50

def daterange(date1, date2):
	from datetime import timedelta, date
	for n in range(int ((date2 - date1).days)+1):
		yield date1 + timedelta(n)

def process_weights(vector, doctor_list):
	"""
	Takes into input the vector for the row and the list of doctor full names

	Returns dictionary of { doctor_name : weight }
	The higher the weight, the more important the doctor is to making the team more efficient
	"""
	pickle_in = open('dataset/LinearRegressionM1.pickle','rb')
	linear_regression_model = pickle.load(pickle_in)

	number_of_doctors = len(doctor_list)

	doctor_vector = vector[:number_of_doctors]  # vector of doctors
	date_vector = vector[number_of_doctors:]    # vector of hour and date
	overall_number_of_doctors_in_selection = len([i for i, x in enumerate(doctor_vector) if x == 1])

	# overall PPH score
	overall_score = linear_regression_model.predict(np.array([vector]))[0]

	# dictionary with the doctor name key associated to the weight value
	doctor_weights = {}

	# get doctor indices
	doctor_indices = [i for i, x in enumerate(doctor_vector) if x == 1]

	for doctor_index in doctor_indices:

		# get doctor's full name
		doctor_full_name = doctor_list[doctor_index]

		# remove the specified doctor from the list (1 becomes 0)
		temp_doctor_vector = doctor_vector.copy()
		temp_doctor_vector[doctor_index] = 0
		combined_vector = temp_doctor_vector + date_vector

		# compute weight
		prediction = linear_regression_model.predict(np.array([combined_vector]))[0]
		number_of_doctors_in_selection = len([i for i, x in enumerate(temp_doctor_vector) if x == 1])
		weight = overall_score/overall_number_of_doctors_in_selection - prediction/number_of_doctors_in_selection

		# assign key and value to dictionary
		doctor_weights[doctor_full_name] = weight

	return doctor_weights

def vectorize_doctors(list_of_doctors_current, list_of_doctors_total):

    doctor_vector = [0]*len(list_of_doctors_total)
    for doctor_name_current in list_of_doctors_current:
        for doctor_name_total in list_of_doctors_total:
            if doctor_name_current == doctor_name_total:
                doctor_vector[list_of_doctors_total.index(doctor_name_current)] = 1

    return doctor_vector

def timeto(hour):
    time_vector = []
    time_vector = [0]*24
    time_vector[hour] = 1
    return (time_vector)

def convert_excel_file_to_pandas(excel_file):
    df_excel_schedule = pd.read_excel(excel_file, header=6, index_col='Dates')
    return df_excel_schedule

def exponentiate_critic(values):
	new_values = []
	for value in values:
		a = ((0.75 * value) - 2.67)
		if a >= 0:
			new_value = (a ** 2) + 2.67
			new_values.append(new_value)
		elif a < 0:
			new_value = -(a ** 2) + 2.67
			new_values.append(new_value)
	return new_values

def exponentiate_medium(values):
	new_values = []
	for value in values:
		a = ((0.85 * value) - 2.67)
		if a >= 0:
			new_value = (a ** 2) + 2.67
			new_values.append(new_value)
		elif a < 0:
			new_value = -(a ** 2) + 2.67
			new_values.append(new_value)
	return new_values

def exponentiate(values):
	new_values = []
	for value in values:
		a = (value - 2.67)
		if a >= 0:
			new_value = (a ** 2) + 2.67
			new_values.append(new_value)
		elif a < 0:
			new_value = -(a ** 2) + 2.67
			new_values.append(new_value)
	return new_values

def backend_predict(list_of_doctors_current, time_military_format, model_name, list_of_doctors_total):
	doctor_vector = vectorize_doctors(list_of_doctors_current=list_of_doctors_current, list_of_doctors_total=list_of_doctors_total)
	time_vector = timeto(time_military_format)

	combined_vector = doctor_vector + time_vector
	combined_vector = np.array([combined_vector])

	pickle_in = open('dataset/LinearRegressionM1.pickle','rb')
	model = pickle.load(pickle_in)
	prediction = model.predict(np.array(combined_vector))

	return prediction


# Configure Flask app
app = Flask(__name__)

list_of_doctors_total_csv = pd.read_csv('list_of_doctors.csv', sep=';', header=None)
list_of_doctors_total_csv = list_of_doctors_total_csv[0].tolist()
list_of_last_names = []
for doctor in list_of_doctors_total_csv:
	list_of_last_names.append(doctor.split(',')[0].strip())

schedule_tables_dict = {}

months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
weekdays = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun']

#Machine Learning Models
pickle_in = open('dataset/LinearRegressionM1.pickle','rb')
model = pickle.load(pickle_in)


#Login Page
@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        if request.form['username'] != 'admin' or request.form['password'] != 'admin':
            error = 'Invalid Credentials. Please try again.'
        else:
            return redirect(url_for('home'))
    return render_template('login.html', error=error)

# read JSON file
with open('doctor_shifts.json') as f:
	doctor_shifts = json.load(f)

#Set Exponentiation thresholds
with open('thresholds.json') as t:
	all_thresholds = json.load(t)

#Home Page
@app.route('/home', methods=['GET', 'POST'])
def home():
	if request.method == "POST":
		schedule_tables_dict.clear()
		schedule_table = ''
		TableID = 0
		ButtonID = 0

		key = 'Emergence'
		print("Reading file...")
		f = request.files["file"]
		df_schedule = convert_excel_file_to_pandas(f)
		df_schedule.drop(columns=["Admin on call", "Backup day", "Backup night"], axis=1, inplace=True)

		thresholds = []

		search_date = datetime.datetime.strptime(request.form["search_date"],"%Y-%m-%d")
		search_date_end = datetime.datetime.strptime(request.form["search_date_end"],"%Y-%m-%d")
		delta = search_date_end - search_date

		#Getting calendar months
		calendar_dict = {}
		calendar_months = []
		all_calendar_days = []
		html_calendars = []

		for dt in daterange(search_date, search_date_end):
			calendar_months.append(dt.strftime("%Y-%m"))
			all_calendar_days.append(dt.strftime("%Y-%m-%d"))

		unique_months = sorted(list(set(calendar_months)))
		for var in unique_months:
			m = months[int(var.split('-')[1].strip()) - 1]
			calendar_dict[m] = calendar_months.count(var)

		for date in unique_months:
			year = int(date.split('-')[0].strip())
			month = int(date.split('-')[1].strip())
			raw = calendar.HTMLCalendar(calendar.SUNDAY)
			raw_calendar = raw.formatmonth(year, month)
			html_calendars.append(raw_calendar)

		#Background colors and DateIDs
		background_color = []
		all_ids = [x for x in range(0, len(all_calendar_days))]

		#Get month vector
		for day in range(delta.days + 1):
			current_date = search_date + datetime.timedelta(day)
			search_date_string = current_date.strftime("%a, %d-%b-%Y").replace(" 0", " ")
			temp_row = df_schedule.ix[search_date_string]
			schedule_col_names = df_schedule.columns

			month = search_date_string.split('-')[1]
			month_index = months.index(month)
			month_vec = [0] * 12
			month_vec[month_index] = 1

			# for each shift in the row
			#fullnames = []

			day_schedule = pd.DataFrame(index=range(24))
			schedule_col_names = df_schedule.columns

			#shift_and_doctor = {}

			for column, value in zip(schedule_col_names, temp_row):
				for ind, lastname in enumerate(list_of_last_names):
					if value.lower() == lastname.lower():
						list_range_hours = doctor_shifts[column]

						thresholds.append(all_thresholds[column])

						start = list_range_hours[0] - 7
						end = list_range_hours[1] - 7

						doctor_full_name_current = list_of_doctors_total_csv[ind]

						for hour in range(start, end):
							day_schedule = day_schedule.set_value(hour, column, doctor_full_name_current)

						#shift_and_doctor[column] = doctor_full_name_current
						#fullnames.append(doctor_full_name_current)

			# vectorize row
			pph_list = []
			pphpd_list = []
			thresholds_list = []
			for index, row in day_schedule.iterrows():
				list_of_thresholds_per_row = []
				list_of_doctors_per_row = []
				for i, doctor in enumerate(row):
					if pd.isnull(doctor) == False:
						list_of_thresholds_per_row.append(thresholds[i])
						list_of_doctors_per_row.append(doctor)

				#Get avergaged threshold for each row
				if len(list_of_thresholds_per_row) != 0:
					thresholds_list.append(sum(list_of_thresholds_per_row) / len(list_of_thresholds_per_row))
				elif len(list_of_thresholds_per_row) == 0:
					thresholds_list.append(3.56)

				vectorized_row_doctors = vectorize_doctors(list_of_doctors_per_row, list_of_doctors_total_csv)
				vectorized_hour = timeto((index+7) % 24)

				vector = vectorized_row_doctors + vectorized_hour + month_vec

				#Predict PPH
				'''
				model = load_model('dataset/ER-Efficiency-model2.h5')
				prediction = model.predict(np.array([vector]))
				#prediction = -1
				'''

				prediction = model.predict(np.array([vector]))

				if len(list_of_doctors_per_row) != 0:
					PPHPD = prediction / len(list_of_doctors_per_row)
					pph_list.append(prediction)
				elif len(list_of_doctors_per_row) == 0:
					PPHPD = 2.67
					pph_list.append([0.0])

				pphpd_list.append(PPHPD)

			pph_list = np.array(pph_list).flatten()
			pph_list = [round(value, 1) for value in pph_list]

			pphpd_list = np.array(pphpd_list).flatten()

			exp_thresholds_list = exponentiate_critic(thresholds_list)
			exp_pphpd_list = exponentiate(pphpd_list)
			thresholds_list = exponentiate_medium(thresholds_list)

			#Get colours Overall Score
			Overall = []
			for pphpd, threshold, critic_threshold in zip(exp_pphpd_list, thresholds_list, exp_thresholds_list):
				if critic_threshold == 2.67 and pphpd == 2.67:
					Overall.append('No Doctor Registered')
				elif pphpd >= threshold:
					Overall.append('Green')
				elif pphpd < threshold and pphpd >= critic_threshold:
					Overall.append('Yellow')
				elif pphpd < critic_threshold:
					Overall.append('Red')

			feature_analysis_button = ''
			ButtonID -= 1
			if Overall.count('Red') >= 2:
				background_color.append('#FF4900')
				feature_analysis_button = f'<button type="button" id="{ButtonID}" style="padding:0px 6px; font-size:15px; border-radius:2px; float:right;" class="btn btn-danger">Analyze</button>'
			elif Overall.count('Yellow') >= 3:
				background_color.append('#FFD400')
				feature_analysis_button = f'<button type="button" id="{ButtonID}" style="padding:0px 6px; font-size:15px; border-radius:2px; float:right;" class="btn btn-warning text-white">Analyze</button>'
			else:
				background_color.append('#19D529')
				feature_analysis_button = ''

			time_index = ['7:00 am', '8:00 am', '9:00 am', '10:00 am', '11:00 am', '12:00 pm',
					'1:00 pm', '2:00 pm', '3:00 pm', '4:00 pm', '5:00 pm', '6:00 pm',
					'7:00 pm', '8:00 pm', '9:00 pm', '10:00 pm', '11:00 pm', '12:00 am',
					'1:00 am', '2:00 am', '3:00 am', '4:00 am', '5:00 am', '6:00 am']

			day_schedule['Time'] = time_index
			day_schedule.set_index('Time', inplace=True)
			day_schedule['PPH score'] = pph_list
			day_schedule['Overall score'] = Overall
			day_schedule.reset_index(inplace=True)
			day_schedule.replace(np.nan, '', regex = True, inplace=True)
			schedule_tables_dict[day] = day_schedule

			'''
			schedule_table_inner = day_schedule.to_html(index = False)

			#Set New Table ID
			TableID += 1

			original_string = """<table border="1" class="dataframe">"""
			new_string = f"""<table style="width=100%; table-layout:fixed;" class="table table-bordered" id="{TableID}">"""
			schedule_table_inner = schedule_table_inner.replace(original_string, new_string)
			schedule_table_inner = schedule_table_inner.replace("""<th>Time</th>""", """<th width=90>Time</th>""")
			schedule_table_inner = schedule_table_inner.replace("""<td>Green</td>""", """<td bgcolor="#19D529"></td>""")
			schedule_table_inner = schedule_table_inner.replace("""<td>Yellow</td>""", """<td bgcolor="#FFD400"></td>""")
			schedule_table_inner = schedule_table_inner.replace("""<td>Red</td>""", """<td bgcolor="#FF4900"></td>""")
			'''

		#Configuring calendar
		originals = []
		replacements = []
		for month, length in calendar_dict.items():
			original_days = []
			new_days = []

			important_ids = all_ids[:length]
			important_days = all_calendar_days[:length]
			important_backgrounds = background_color[:length]

			for index, date in enumerate(important_days):
				y, m, day = (int(x) for x in date.split('-'))
				weekday = weekdays[datetime.date(y, m, day).weekday()]
				bgcolor = important_backgrounds[index]
				id_ = important_ids[index]

				original_days.append(f"""<td class="{weekday}">{day}</td>""")
				new_days.append(f"""<td id="{id_}" bgcolor="{bgcolor}" class="{weekday}"><button type="button">{day}</button></td>""")

			originals.append(original_days)
			replacements.append(new_days)

			all_calendar_days = all_calendar_days[length:]
			background_color = background_color[length:]
			all_ids = all_ids[length:]


		for index, cal in enumerate(html_calendars):
			original_string = """<table border="0" cellpadding="0" cellspacing="0" class="month">"""
			new_string = """<table style="width=100%; table-layout:fixed;" class="table table-bordered">"""
			cal = cal.replace(original_string, new_string)

			orig = originals[index]
			repl = replacements[index]

			for i, o in enumerate(orig):
				r = repl[i]
				cal = cal.replace(o, r)

			html_calendars[index] = cal

		return render_template('schedule.html', calendar = html_calendars, key = key)

	return render_template('home.html')

#Feature Analysis
@app.route('/analysis', methods=['GET'])
def analysis():
	return render_template('analysis.html')

#Schedule
@app.route('/schedule', methods=['GET', 'POST'])
def schedule():
	if request.method == "POST":
		dateID = request.form['dateID']
		getmonth = request.form['month']

		raw_table = schedule_tables_dict[int(dateID)].copy()

		#Month Vector
		month_vector = [0] * 12
		month = getmonth[:3]
		month_index = months.index(month)
		month_vector[month_index] = 1

		for index, row in raw_table.iterrows():
			if row['Overall score'] == 'Red':
				#Time Vector
				time_vector = [0] * 24
				time = (int(row['Time'].split(':')[0].strip()) - 7) % 24
				time_vector[time] = 1

				doctor_row = row.drop(labels=['Time', 'PPH score', 'Overall score'])
				doctor_vector = [0] * len(list_of_doctors_total_csv)
				for doctor_name in doctor_row:
					doctor_last_name = doctor_name.split(',')[0]
					if any(doctor_last_name.lower() == doctor_full_name.lower().split(',')[0] for doctor_full_name in list_of_doctors_total_csv):
						doctor_index = list_of_doctors_total_csv.index(doctor_name)
						doctor_vector[doctor_index] = 1

				#If number of doctors in row is equal to 1
				if doctor_vector.count(1) == 1:
					new_doctor_name = '!' + doctor_name +'!!'
					new_row = row.replace(doctor_name, new_doctor_name)
					raw_table.ix[index] = new_row

				#If number of doctors is superior to 1
				else:
					#print(doctor_vector, time_vector, month_vector)
					input_vector = doctor_vector + time_vector + month_vector
					doctor_weights = process_weights(input_vector, list_of_doctors_total_csv)

					for doctor, weight in doctor_weights.items():
						if weight > 0:
							new_doctor_name = '!' + str(doctor) + '!!'
							row.replace(doctor, new_doctor_name, inplace=True)

					highest = max(doctor_weights.items(), key=operator.itemgetter(1))[0]
					new_doctor_name = '!' + str(highest) +'!!'
					row.replace(highest, new_doctor_name, inplace=True)

					raw_table.ix[index] = row

		html_table = raw_table.to_html(index=False)

		original_string = """<table border="1" class="dataframe">"""
		new_string = """<table style="width:auto; table-layout:fixed;" class="table table-bordered">"""
		html_table = html_table.replace(original_string, new_string)
		html_table = html_table.replace("""!""", """<p>""")
		html_table = html_table.replace("""!!""", """<p>""")
		html_table = html_table.replace("""<th>Time</th>""", """<th width=90>Time</th>""")
		html_table = html_table.replace("""<td>Green</td>""", """<td bgcolor="#19D529"></td>""")
		html_table = html_table.replace("""<td>Yellow</td>""", """<td bgcolor="#FFD400"></td>""")
		html_table = html_table.replace("""<td>Red</td>""", """<td bgcolor="#FF4900"></td>""")

		return render_template('analysis.html', html_table = html_table)
	return render_template('schedule.html')

# Calculator display
@app.route('/calculator', methods=["GET", "POST"])
def calculator():
	return render_template("calculator.html",  list_of_doctors=list_of_doctors_total_csv)

@app.route("/display", methods=["GET", "POST"])
def display():
    # get doctor names
    list_of_doctors_current = []
    form_names = ["doctor_1", "doctor_2", "doctor_3", "doctor_4", "doctor_5"]
    for form_name in form_names:
        list_of_doctors_current.append(request.form[form_name])

    # get time format
    time_military_format = request.form["hour_of_day"]
    print(time_military_format)
    time_military_format = int(time_military_format.split(':')[0])

    print(list_of_doctors_current)
    print(time_military_format)

    # predict
    prediction = backend_predict(list_of_doctors_current, time_military_format, 'dataset/ER-Efficiency-model1.h5', list_of_doctors_total_csv)

    list_of_doctors_current = []
    time_military_format = 0

    print("ABOUT TO RENDER TEMPLATE!")
    print(prediction)
    return render_template("display.html", prediction=prediction[0])


# Feedback code

@app.route("/feedback")
def feedback():
	"""
	Render HTML template for doctor Feedback
	"""
	print(list(all_thresholds.keys()))
	list_of_all_shifts = list(all_thresholds.keys())
	print(list_of_doctors_total_csv)
	return render_template("input_feedback.html", list_of_doctors=list_of_doctors_total_csv, list_of_shifts=list_of_all_shifts)


@app.route("/compute_feedback", methods=["GET", "POST"])
def compute_feedback():
	if request.method == 'POST':

		# read contents from feedback() function
		doctor_name = request.form["doctor_name"]
		shift_name = request.form["shift_name"]
		date = request.form["day"]
		submit_time = request.form["shift_time"]
		satisfaction = request.form["satisfaction"]
		stress = request.form["stress"]
		tired = request.form["tired"]
		comment = request.form["comment"]

		csv_row = [doctor_name, shift_name, date, submit_time, satisfaction, stress, tired, comment]
		with open('dataset/feedback.csv', 'a') as f:
			writer = csv.writer(f)
			writer.writerow(csv_row)

		return render_template("success_message.html", doctor_name=doctor_name)


if __name__ == '__main__':
    app.run(debug=True)
