"""
Flask app file to be run to render web app
"""

# Import modules
from flask import Flask, flash, redirect, url_for, render_template, request, Response, send_file, make_response, abort
from werkzeug.utils import secure_filename
from flask_sqlalchemy import SQLAlchemy
import pandas as pd
import matplotlib.pyplot as plt

import glob
import os
import re

# from keras.models import load_model

import collections
import numpy as np
import pandas as pd
import string
import csv
import json
import datetime
import math
import pickle
import operator
import calendar
import copy

from functions import *

# Configure Flask app and SQL database
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///users.db'

# Profile pics
UPLOAD_FOLDER = 'Static'
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

SQLALCHEMY_TRACK_MODIFICATIONS = False
db = SQLAlchemy(app) 	# database

class Physician(db.Model):
	"""Object for Physician"""
	id = db.Column(db.Integer, primary_key=True)
	username = db.Column(db.String(80), unique=True, nullable=False)
	password = db.Column(db.String(60), nullable=False)
	admin = db.Column(db.Boolean)
	email = db.Column(db.String(120), unique=True, nullable=False)
	image_file = db.Column(db.String(20), nullable=False, default='doctor_cartoon.jpg')

	def __repr__(self):
		return f"Physician('{self.username}', '{self.email}', '{self.image_file}')"

class Permanent_Shifts(db.Model):
	"""Permanent Shifts"""
	id = db.Column(db.Integer, primary_key=True)
	ShiftName = db.Column(db.String(80), nullable=False)
	StartTime = db.Column(db.Integer)
	EndTime = db.Column(db.Integer)

	def __repr__(self):
		return f"DoctorShifts('{self.ShiftName}', '{self.StartTime}', '{self.EndTime}')"

class Shifts(db.Model):
	"""Every row: ID, date, doctor name, shift"""
	id = db.Column(db.Integer, primary_key=True)
	Date = db.Column(db.String(100), nullable=False)
	DoctorName = db.Column(db.String(80), nullable=False)
	ShiftName = db.Column(db.String(80), nullable=False)

	def __repr__(self):
		return f"Shifts('{self.Date}', '{self.DoctorName}', '{self.ShiftName}')"

class Unavailable_Days(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	DoctorName = db.Column(db.String(80), nullable=False)
	Date = db.Column(db.String(80), nullable=False)

	def __repr__(self):
		return f"Unavailable_Days('{self.DoctorName}', '{self.Date}')"

class Unavailable_Hours(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	DoctorName = db.Column(db.String(80), nullable=False)
	Weekday = db.Column(db.Integer)
	Time = db.Column(db.Integer)

	def __repr__(self):
		return f"Hours('{self.DoctorName}', '{self.Weekday}', '{self.Time}')"

class Restrictions(db.Model):
	"""Restrictions"""
	id = db.Column(db.Integer, primary_key=True)
	DoctorName = db.Column(db.String(80), nullable=False)
	TotalHours = db.Column(db.Integer)
	HoursPerWeek = db.Column(db.Integer)
	NumberDayShifts = db.Column(db.Integer)
	NumberNightShifts = db.Column(db.Integer)
	NumberBackupShifts = db.Column(db.Integer)
	NumberWeekends = db.Column(db.Integer)
	# TotalMinimumHours = db.Column(db.Integer)

	def __repr__(self):
		return f"Restrictions('{self.DoctorName}', '{self.TotalHours}', '{self.HoursPerWeek}', '{self.NumberDayShifts}', '{self.NumberNightShifts}', '{self.NumberBackupShifts}', '{self.NumberWeekends}')"


#list of all doctors in Emergency Department
list_of_doctors_total_csv = pd.read_csv('list_of_doctors.csv', sep=';', header=None)
list_of_doctors_total_csv = list_of_doctors_total_csv[0].tolist()

list_of_last_names = []
for doctor in list_of_doctors_total_csv:
	list_of_last_names.append(doctor.split(',')[0].strip().lower())

# read JSON file
with open('shift_types.json') as w:
	shift_types = json.load(w)

with open('shift_restrictions_yesterday.json') as y:
	shift_restrictions_y = json.load(y)

with open('shift_restrictions_tomorrow.json') as rt:
	shift_restrictions_t = json.load(rt)	

# global variables
username = ''
queryname = ''
login_parameters = []

patient_inflow = -1
patients_seen = -1

preference_ids = []
schedule_tables_dict = {}
proposed_tables_dict = {}
plus_minus_list = []				           # list of patients seen plus or minus
global_pi_list = []
global_pph_list = []

BBavailable_doctors = []
BBavailable_shifts = []

pph_minus_pi_difference_list = []					# list of difference between PPH and PI
list_days_index_pph_minus_pi_ascending_order = []	# index of pandas dictionary of day or days with worst (PPH - PI) value

global_search_date = datetime.datetime.now()
global_search_date_end = datetime.datetime.now()

#Generalized Time, Weekday and Month
time_index = ['7:00 am', '8:00 am', '9:00 am', '10:00 am', '11:00 am', '12:00 pm',
		'1:00 pm', '2:00 pm', '3:00 pm', '4:00 pm', '5:00 pm', '6:00 pm',
		'7:00 pm', '8:00 pm', '9:00 pm', '10:00 pm', '11:00 pm', '12:00 am',
		'1:00 am', '2:00 am', '3:00 am', '4:00 am', '5:00 am', '6:00 am']
months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
weekdays = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun']
complete_weekdays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']

#Track Restrictions
backup_shifts = {}					#Track number of backup shifts
weekends = {}						#Track number of weekends
night_shifts = {}					#Track number of night shifts
day_shifts = {}						#Track number of day shifts
total_hours = {} 					#Check total hours worked by physician

#Define Shift Importance
permanent_shifts_PPH = ['West night', 'Hub eve 5-1', 'Hub eve 6-2', 'West eve 4-12', 'East eve 4-12', 'Hub day 9-5', 'Hub day 10-6', 'East day 7-5', 'West day 7-5', 'X night 8-4']

# shifts that are less important/primary
X_night = ['X night 9-3', 'X night 9-5']
surges = ['Surge 8-2', 'Surge 8-4', 'Surge 10-4', 'Surge 10-6', 'Surge 2-10', 'Surge 4-10', 'Surge 6-12']
helper_shifts = ['ST day 12-8', 'ST eve 8-2', 'EW 10-5 float']

secondary_shifts_list = X_night + surges + helper_shifts


#Machine Learning Models
pickle_in_one = open('Model/Models/LinearRegressionM1.pickle','rb')		# PPH
pickle_in_two = open('Model/Models/Patient_Inflow_Model1.sav', 'rb')	# PI

model = pickle.load(pickle_in_one)
model1 = pickle.load(pickle_in_two)

# global parameter to render login animation template
render_login_animation = True


#LOGIN PAGE
@app.route('/', methods=['GET', 'POST'])
@app.route('/login', methods=['GET', 'POST'])
def login():
	error = None
	global username
	global render_login_animation
	doctor_shifts = {item.ShiftName: [item.StartTime, item.EndTime] for item in Permanent_Shifts.query.all()}

	if request.method == 'POST':
		username = (request.form['doctor_name']).split(',')[0].strip().lower()
		password = request.form['password']
		user = Physician.query.filter_by(username=username).first()

		if user:
			if user.username != username:
				error = 'Invalid credentials. Please try again!'	
			else:
				true_password = user.password
				if password == true_password:
					all_shifts = Shifts.query.filter_by(DoctorName=username.split(',')[0].strip().lower()).all()
					fullname = list_of_doctors_total_csv[list_of_last_names.index(username)]
					image_file = (Physician.query.filter_by(username=username).first()).image_file
					#today = str(datetime.datetime.now()).split(' ')[0]
					#tomorrow = str(datetime.date.today() + datetime.timedelta(days=1))
					today = '2017-04-10'
					SHIFT_ONE = 'No work today!'
					SHIFT_TWO = 'No work!'
					start_time_one = 'None'
					start_time_two = 'None'
					end_time_one = 'None'
					end_time_two = 'None'
					for shift in all_shifts:
						if shift.Date == today:
							SHIFT_ONE = shift.ShiftName
							start_time_one = str(doctor_shifts[SHIFT_ONE][0] % 24) + ':00'
							end_time_one = str(doctor_shifts[SHIFT_ONE][1] % 24) + ':00'

					shifts_dates_AT = [datetime.datetime.strptime(x.Date, "%Y-%m-%d") for x in all_shifts if x.Date > today]
					shift_names_AT = [x.ShiftName for x in all_shifts if x.Date > today]

					shift_date, shift_index = nearest_date(shifts_dates_AT, datetime.datetime.strptime(today, "%Y-%m-%d"))
					shift_date = str(shift_date).split(' ')[0]
					SHIFT_TWO = shift_names_AT[shift_index]
					start_time_two = str(doctor_shifts[SHIFT_TWO][0] % 24) + ':00'
					end_time_two = str(doctor_shifts[SHIFT_TWO][1] % 24) + ':00'
					login_parameters.extend((today, shift_date, SHIFT_ONE, SHIFT_TWO, start_time_one, end_time_one, start_time_two, end_time_two))
					return render_template('doctor_home.html', doctor_name=fullname, image_file=image_file, date_one=today, date_two=shift_date, one=SHIFT_ONE, two=SHIFT_TWO, sto=start_time_one, eto=end_time_one, stt=start_time_two, ett=end_time_two)
		else:
			if username == 'admin' and password == 'admin':
				return redirect(url_for('home'))
			else:
				error = 'Invalid credentials. Please try again!'

	if render_login_animation:
		render_login_animation = False
		return render_template('login.html', error=error, list_of_doctors=list_of_doctors_total_csv)
	else:
		return render_template('login_without_animation.html', error=error, list_of_doctors=list_of_doctors_total_csv)





#ADMINISTRATOR


#Home Page
@app.route('/home', methods=['GET', 'POST'])
def home():
	if request.method == "POST":
		global schedule_tables_dict
		global proposed_tables_dict
		global plus_minus_list

		schedule_tables_dict.clear()
		proposed_tables_dict.clear()

		global BBavailable_doctors
		global BBavailable_shifts

		key = 'Emergence'

		f = request.files["file"]
		df_schedule = pd.read_excel(f, header=6)
		df_schedule.set_index('Dates', inplace=True)
		doctor_shifts = {str(item.ShiftName): [item.StartTime, item.EndTime] for item in Permanent_Shifts.query.all()}

		#add_shifts_to_database(db, Shifts, df_schedule, list_of_doctors_total_csv)

		global global_search_date
		global global_search_date_end

		# search dates
		search_date = datetime.datetime.strptime(request.form["search_date"],"%Y-%m-%d")
		search_date_end = datetime.datetime.strptime(request.form["search_date_end"],"%Y-%m-%d")
		global_search_date = search_date
		global_search_date_end = search_date_end

		calendar_dict, all_calendar_days, html_calendars = get_calendars(search_date, search_date_end, months)

		global backup_shifts 
		global weekends 
		global night_shifts
		global day_shifts
		global total_hours
		global pph_minus_pi_difference_list
		global list_days_index_pph_minus_pi_ascending_order

		global patient_inflow
		global patients_seen

		BBavailable_doctors, BBavailable_shifts = get_BB_doctors_and_shifts(df_schedule, list_of_last_names, list_of_doctors_total_csv, BBavailable_doctors, BBavailable_shifts, search_date, all_calendar_days)

		print('Creating Restriction Trackers...')									#Restrictions Trackers
		backup_shifts = {name: 0 for name in BBavailable_doctors}					#Track number of backup shifts
		weekends = {name: 0 for name in BBavailable_doctors}						#Track number of weekends
		night_shifts = {name: 0 for name in BBavailable_doctors}					#Track number of night shifts
		day_shifts = {name: 0 for name in BBavailable_doctors}						#Track number of day shifts
		total_hours = {name: 0 for name in BBavailable_doctors} 					#Check total hours worked by physician

		#Background colors and DateIDs
		background_color = []
		all_ids = [x for x in range(0, len(all_calendar_days))]

		pph_sum_list = []
		pi_sum_list = []

		for day, date in enumerate(all_calendar_days):
			current_date = search_date + datetime.timedelta(day)
			search_date_string = current_date.strftime("%a, %d-%b-%Y").replace(" 0", " ")
			temp_row = df_schedule.ix[search_date_string]
			schedule_col_names = df_schedule.columns

			month = search_date_string.split('-')[1]
			month_vec = monthto(months, month)

			day_schedule = pd.DataFrame(index=range(24))
			day_schedule, doctors, shifts = fill_BB_day_schedule(schedule_col_names, temp_row, list_of_last_names, doctor_shifts, list_of_doctors_total_csv, day_schedule)
			day_schedule_compute = day_schedule.copy()
			
			for backup_shift in ['Admin on call', 'Backup day', 'Backup night']:
				if backup_shift in day_schedule.columns:
					day_schedule_compute.drop(columns=backup_shift, inplace=True)

			for doctor, shift in zip(doctors, shifts): update_BB_restrictions(doctor, shift, date, backup_shifts, weekends, night_shifts, day_shifts, total_hours, doctor_shifts, shift_types)

			# vectorize row
			pph_list, pi_list = vectorize_row_with_PIP(date, day_schedule_compute, month_vec, model, model1, list_of_doctors_total_csv)

			pph_sum = sum(pph_list)
			pi_sum = sum(pi_list)
			pph_sum_list.append(round(pph_sum))
			pi_sum_list.append(round(pi_sum))
			pph_minus_pi_difference_list.append(round(pph_sum) - round(pi_sum))

			#Get colours Overall Score
			Overall = calculate_overall_score_with_PIP(pph_list, pi_list)
			
			if pph_sum >= pi_sum: background_color.append('#19D529')				#Green
			elif pph_sum >= (0.8 * pi_sum): background_color.append('#FFD400')		#Yellow
			else: background_color.append('#FF4900')								#Red 

			day_schedule.insert(loc=0, column='Time', value=time_index)
			day_schedule['PPH score'] = pph_list
			day_schedule['Overall score'] = Overall
			day_schedule.replace(np.nan, '', regex = True, inplace=True)
			schedule_tables_dict[day] = day_schedule

		#Configuring calendar
		originals, replacements = configure_calendars(calendar_dict, all_ids, all_calendar_days, background_color, weekdays)
		html_calendars = configure_html_calendars(originals, replacements, html_calendars)
		carousel_html_calendars = add_carousel_to_html_calendar_table(html_calendars)

		plus_minus_list = pph_sum_list
		patients_seen = str(int(round(sum(plus_minus_list))))
		patient_inflow = str(int(round(sum(pi_sum_list))))

		list_days_index_pph_minus_pi_ascending_order = return_list_of_indices_ascending_order_pph_minus_pi(pph_minus_pi_difference_list)
		'''
		x_axis = [x for x in range(len(pph_sum_list))]
		plt.plot(x_axis, pph_sum_list, label="PPH")
		plt.plot(x_axis, pi_sum_list, label="PI")
		plt.legend()
<<<<<<< HEAD
=======

		plt.savefig('BB_PPH_vs_PI.png', dpi=500)
		plt.savefig('BB_PPH_vs_PI.png', dpi=500)
>>>>>>> 0a484320949f8b4be9312f1ad77c98c72f0956d9
		plt.savefig("Original_schedule_pph_and_pi_values.png", dpi=500)
		plt.close()
		'''
		return render_template('schedule.html', calendar = carousel_html_calendars, patient_inflow=patient_inflow, patients_seen=patients_seen, key = key)
	return render_template('home.html')



#CREATING SCHEDULES FROM SCRATCH

#Configure Shifts
@app.route('/shift_configuration', methods=['GET', 'POST'])
def configure_shifts():
	"""Modify shift names and durations"""

	if request.method == 'POST':
		for item in Permanent_Shifts.query.all():
			newShiftName = request.form[item.ShiftName]
			newStartTime = int((request.form[item.ShiftName + ' Start Time']).split(':')[0])
			newEndTime = int((request.form[item.ShiftName + ' End Time']).split(':')[0])
			if newStartTime < 7: newStartTime += 24
			if newEndTime <= 7: newEndTime += 24
			item.ShiftName = newShiftName
			item.StartTime = newStartTime
			item.EndTime = newEndTime

		db.session.commit()

	permanent_shifts = {item.ShiftName: [str(item.StartTime % 24) , str(item.EndTime % 24)] for item in Permanent_Shifts.query.all()}
	for shiftname, time in permanent_shifts.items():
		if len(time[0]) == 1: time[0] = '0' + time[0]
		if len(time[1]) == 1: time[1] = '0' + time[1]		

	return render_template('shift_configuration.html', doctor_shifts=permanent_shifts)

#Restrictions
@app.route('/update_restrictions', methods=['GET', 'POST'])
def update_restrictions():
	"""Updating the SQL databases viewed from the admin page"""
	global queryname
	global preference_ids
	
	doctor_shifts = {item.ShiftName: [item.StartTime, item.EndTime] for item in Permanent_Shifts.query.all()}
	preferences_table = pd.DataFrame(index=range(24), columns=complete_weekdays)

	if request.method == 'POST':
		Unavailabilities = [x for x in preference_ids if request.form.get(x) == 'on']
		ADMIN = request.form.get('admin')
		TH = request.form['total-hours']
		HPW = request.form['hours-per-week']
		DS = request.form['day-shifts']
		NS = request.form['night-shifts']
		BS = request.form['backup-shifts']
		NW = request.form['weekends']

		physician = Physician.query.filter_by(username=queryname.split(',')[0].strip().lower()).first()
		past_restrictions = Restrictions.query.filter_by(DoctorName=queryname.split(',')[0].strip().lower()).first()
		past_unavailabilities = Unavailable_Hours.query.filter_by(DoctorName=queryname.split(',')[0].strip().lower()).all()

		if ADMIN == 'on': physician.admin = True
		elif ADMIN is None: physician.admin = False

		past_restrictions.TotalHours = TH
		past_restrictions.HoursPerWeek = HPW
		past_restrictions.NumberDayShifts = DS
		past_restrictions.NumberNightShifts = NS
		past_restrictions.NumberBackupShifts = BS
		past_restrictions.NumberWeekends = NW

		for PR in past_unavailabilities:
			db.session.delete(PR)

		for UNA in Unavailabilities:
			weekday = complete_weekdays.index(UNA.split(' ')[0].strip())
			time = int(UNA.split(' ')[1]) + 7
			NR = Unavailable_Hours(DoctorName=queryname.split(',')[0].strip().lower(), Weekday=weekday, Time=time)
			db.session.add(NR)
		
		db.session.commit()

	physician = Physician.query.filter_by(username=queryname.split(',')[0].strip().lower()).first()
	all_unavailable_hours = Unavailable_Hours.query.filter_by(DoctorName=queryname.split(',')[0].strip().lower()).all()
	doctor_restrictions = Restrictions.query.filter_by(DoctorName=queryname.split(',')[0].strip().lower()).first()

	ADMIN = physician.admin
	TH = doctor_restrictions.TotalHours
	HPW = doctor_restrictions.HoursPerWeek
	DS = doctor_restrictions.NumberDayShifts
	NS = doctor_restrictions.NumberNightShifts
	BS = doctor_restrictions.NumberBackupShifts
	NW = doctor_restrictions.NumberWeekends

	available_shifts = []	# list of available shifts
	ids = []				# list of ID for database, analogy to enumerate()
	red_ids = []			# when a box is red, and keep it red

	for weekday in preferences_table.columns.values:
		TS = []
		for shift, time in doctor_shifts.items():
			TS.append(shift)
			for UN in all_unavailable_hours:
				if UN.Weekday == complete_weekdays.index(weekday) and time[0] <= UN.Time <= time[1] and shift in TS:
					TS.remove(shift)
		available_shifts.extend(TS)

	for index, hour in preferences_table.iterrows():
		for i, weekday in enumerate(hour):
			preferences_table.set_value(index, complete_weekdays[i], complete_weekdays[i] + ' ' + str(index))
			ids.append(complete_weekdays[i] + ' ' + str(index))

			for UN in all_unavailable_hours:
				if UN.Weekday == i and int(UN.Time) - 7 == index:
					preferences_table.set_value(index, complete_weekdays[i], complete_weekdays[i] + ' ' + str(index) + ' ' + 'red')
					red_ids.append(complete_weekdays[i] + ' ' + str(index) + ' ' + 'red')


	preferences_table.insert(loc=0, column='Time', value=time_index)
	preferences_table.replace(np.nan, '', regex=True, inplace=True)

	html_table = preferences_table.to_html(index=False)
	original_string = """<table border="1" class="dataframe">"""
	new_string = """<table style="width:90%; table-layout:fixed; margin-left:auto; margin-right:auto" class="table table-bordered table-sm">"""
	html_table = html_table.replace(original_string, new_string)

	# Display HTML table
	for id_ in ids:
		html_table = html_table.replace(f"""<td>{id_}</td>""", f"""<td id='{id_}'><input type="checkbox" ID='tablecheckbox' name="{id_}"></td>""")
	for id_ in red_ids:
		html_table = html_table.replace(f"""<td>{id_}</td>""", f"""<td id='{id_}' bgcolor='#FF4900'><input type="checkbox" ID='tablecheckbox' name="{id_}" checked></td>""")	

	# list of IDs
	preference_ids.clear()
	all_ids = ids + red_ids
	preference_ids = [x for x in all_ids]

	return render_template('restrictions.html', preference_table=html_table, all_ids=all_ids, doctor_name=queryname, TH=TH, HPW=HPW, DS=DS, NS=NS, BS=BS, NW=NW, ADMIN=ADMIN, unavailable_days=None)


@app.route('/restrictions', methods=['GET', 'POST'])
def restrictions():
	"""Update restrictions"""
	if request.method == 'POST':
		global queryname
		username = request.form['doctor_name']
		queryname = username
		return redirect(url_for('update_restrictions'))
	return render_template('restrictions.html', query=list_of_doctors_total_csv)


@app.route('/create_schedule', methods=['GET'])
def create_schedule():
	"""Create schedule function"""
	doctor_shifts = {item.ShiftName: [item.StartTime, item.EndTime] for item in Permanent_Shifts.query.all()}
	return render_template('create_schedule.html', list_of_doctors=list_of_doctors_total_csv, list_of_shifts=doctor_shifts)

@app.route('/set_schedule', methods=['GET', 'POST'])
def set_schedule():
	"""Create schedule from scratch"""
	if request.method == 'POST':
		doctor_shifts = {item.ShiftName: [item.StartTime, item.EndTime] for item in Permanent_Shifts.query.all()}
		schedule_start = datetime.datetime.strptime(request.form['start_date'], "%Y-%m-%d")
		schedule_end = datetime.datetime.strptime(request.form['end_date'], "%Y-%m-%d")
		available_doctors = [x.split(',')[0].strip().lower() for x in list_of_doctors_total_csv if request.form.get(x.split(',')[0]) == 'on']
		available_shifts = [x for x in doctor_shifts if request.form.get(x) == 'on']
		all_add_on_shifts = X_night + surges + helper_shifts

		print('Preparing Backup Shifts...')
		available_admin = [name for name in available_doctors if (Physician.query.filter_by(username=name).first()).admin == True]

		print('Retrieving Restrictions...')																										#Restrictions
		max_hours = {name: (Restrictions.query.filter_by(DoctorName=name).first()).TotalHours for name in available_doctors}                    #Max Number of Hours on 10 week bloc
		max_limit = {name: (Restrictions.query.filter_by(DoctorName=name).first()).HoursPerWeek for name in available_doctors}					#Max Number Of Hours
		max_backup_shifts = {name: (Restrictions.query.filter_by(DoctorName=name).first()).NumberBackupShifts for name in available_doctors}	#Max Number Backup Shifts
		max_weekends = {name: (Restrictions.query.filter_by(DoctorName=name).first()).NumberWeekends for name in available_doctors}				#Max Number Weekends
		max_night_shifts = {name: (Restrictions.query.filter_by(DoctorName=name).first()).NumberNightShifts for name in available_doctors}		#Max Number Night Shifts
		max_day_shifts = {name: (Restrictions.query.filter_by(DoctorName=name).first()).NumberDayShifts for name in available_doctors}			#Max Number Day Shifts
		unavailable_hours = {name: (Unavailable_Hours.query.filter_by(DoctorName=name).all()) for name in available_doctors}					#Unavailable Hours

		global backup_shifts
		global weekends
		global night_shifts
		global day_shifts
		global total_hours

		print('Creating Restriction Trackers...')								#Restrictions Trackers
		backup_shifts = {name: 0 for name in available_doctors}					#Track number of backup shifts
		weekends = {name: 0 for name in available_doctors}						#Track number of weekends
		night_shifts = {name: 0 for name in available_doctors}					#Track number of night shifts
		day_shifts = {name: 0 for name in available_doctors}					#Track number of day shifts
		doctor_workload = {name: 0 for name in available_doctors}				#Track hours worked per week
		total_hours = {name: 0 for name in available_doctors} 					#Check total hours worked by physician

		#Configuring Calendar and Variables
		global proposed_tables_dict
		global plus_minus_list
		proposed_tables_dict.clear()
		calendar_dict, all_calendar_days, html_calendars = get_calendars(schedule_start, schedule_end, months)
		proposed_tables_dict = create_schedule_df_dict(all_calendar_days)

		all_ids = [x for x in range(0, len(all_calendar_days))]
		key = 'Emergence'
		background_color = []
		counter = 0
		pph_sum_list = []
		pi_sum_list = []

		#Rearrange all_calendar_days from lowest PPH to highest
		for day, date in enumerate(all_calendar_days):
			if counter != 0 and counter != len(all_calendar_days) - 1:
				previous_day_schedule = proposed_tables_dict[day - 1]
				next_day_schedule = proposed_tables_dict[day + 1]
			elif counter == 0:
				previous_day_schedule = None
				next_day_schedule = proposed_tables_dict[day + 1]
			elif counter == len(all_calendar_days):
				previous_day_schedule = proposed_tables_dict[day - 1]
				next_day_schedule = None

			og_dict = update_list_of_available_doctors_with_unavailable_hours(previous_day_schedule, next_day_schedule, doctor_workload, total_hours, available_shifts, available_doctors, shift_restrictions_y, shift_restrictions_t, doctor_shifts, day_shifts, night_shifts, weekends, shift_types, backup_shifts, max_limit, max_hours, max_backup_shifts, max_day_shifts, max_night_shifts, max_weekends, date, unavailable_hours)
			day_schedule, pph_sum, pi_sum, random_doctor_row = create_basic_doctor_rows(date, og_dict, permanent_shifts_PPH, all_add_on_shifts, list_of_last_names, weekends, backup_shifts, night_shifts, day_shifts, shift_types, doctor_workload, total_hours, doctor_shifts, list_of_doctors_total_csv, model, model1, time_index)
			day_schedule = add_backup_shifts(date, day_schedule, random_doctor_row, available_admin, list_of_doctors_total_csv, list_of_last_names, og_dict, doctor_shifts, backup_shifts, weekends, Restrictions)

			if counter % 7 == 0:
				doctor_workload = reinitialize_doctor_workload(doctor_workload)

			#if Overall.count('Red') >= 1 or Overall.count('Yellow') >= 3:
			if pi_sum > pph_sum: background_color.append('#FFD400')
			else: background_color.append('#19D529')

			proposed_tables_dict[day] = day_schedule
			pph_sum_list.append(pph_sum)
			pi_sum_list.append(pi_sum)
			counter += 1

		total_plus_minus = []
		for pph, pi in zip(pph_sum_list, pi_sum_list):
			total_plus_minus.append(round(pph - pi))

		# Patient inflow and patient per hour metrics
		patient_inflow = int(round(sum(pi_sum_list)))
		patients_seen = int(round(sum(pph_sum_list)))
		plus_minus = round(sum(total_plus_minus))
		plus_minus_list = total_plus_minus

		'''
		x_axis = [x for x in range(len(pph_sum_list))]
		plt.plot(x_axis, pph_sum_list, label="PPH")
		plt.plot(x_axis, pi_sum_list, label="PI")
		plt.legend()
		plt.savefig("test_results_pph_pi.png", dpi=500)
		'''
		#Configuring calendar
		originals, replacements = configure_calendars(calendar_dict, all_ids, all_calendar_days, background_color, weekdays)
		html_calendars = configure_html_calendars(originals, replacements, html_calendars)
		carousel_html_calendars = add_carousel_to_html_calendar_table(html_calendars)

		return render_template('optimized_schedule.html', calendar=carousel_html_calendars, patient_inflow=patient_inflow, patients_seen=patients_seen, total_plus_minus = plus_minus, key=key)
	return abort(404)



#DISPLAYING CALENDARS AND ANALYSIS PAGES
@app.route('/analysis', methods=['GET'])
def analysis():
	"""Displays schedule for a specifc day"""
	return render_template('analysis.html')

#Schedule
@app.route('/schedule', methods=['GET', 'POST'])
def schedule():
	"""Displays calendar for specific month"""

	# if POST request, display schedule for specific day
	if request.method == "POST":
		global schedule_tables_dict
		date = request.form['date']
		dateID = request.form['dateID']
		getmonth = request.form['month']

		month = getmonth[:3]
		month_vector = monthto(months, month)
		current_date = str(getmonth.split(' ')[0].strip()) + ' ' + str(date) + ' ' + str(getmonth.split(' ')[1].strip())

		raw_table = copy.deepcopy(schedule_tables_dict[int(dateID)])
		raw_table = feature_importance(list_of_doctors_total_csv, raw_table, month_vector, model)
		html_table = schedule_df_to_html(raw_table)

		return render_template('analysis.html', current_date = current_date, html_table = html_table)
		
	global patient_inflow
	global patients_seen

	print(patient_inflow)
	print(patients_seen)

	return render_template('schedule.html', patient_inflow=patient_inflow, patients_seen=patients_seen)


@app.route('/optimized_analysis', methods=['GET'])
def optimized_analysis():
	"""Display optimized analysis"""
	return render_template('optimized_analysis.html')

@app.route('/show_optimized_calendar', methods=['GET', 'POST'])
def show():
	"""Display calendar for specific month"""

	# if POST request, display schedule for specific day
	if request.method == "POST":
		global schedule_tables_dict 
		global proposed_tables_dict
		global plus_minus_list

		date = request.form['date']
		dateID = request.form['dateID']
		getmonth = request.form['month']
		current_date = str(getmonth.split(' ')[0].strip()) + ' ' + str(date) + ' ' + str(getmonth.split(' ')[1].strip())

		html_table = schedule_df_to_html(copy.deepcopy(proposed_tables_dict[int(dateID)]))
		plus_minus = str(plus_minus_list[int(dateID)])

		return render_template('optimized_analysis.html', current_date = current_date, html_table = html_table, plus_minus = plus_minus)
	return render_template('optimized_schedule.html')

@app.route('/optimized_BB_analysis', methods=['GET'])
def optimized_BB_analysis():
	"""Optimized ByteBloc schedule"""
	return render_template('optimized_BB_analysis.html')

@app.route('/show_BB_optimized_calendar', methods=['GET', 'POST'])
def show_BB():
	"""Show schedule for particular date"""
	if request.method == "POST":
		global schedule_tables_dict  
		global proposed_tables_dict
		global plus_minus_list
		global global_pph_list
		global global_pi_list

		date = request.form['date']
		dateID = request.form['dateID']			# date ID corresponds to the index of a date. Is a string
		getmonth = request.form['month']
		current_date = str(getmonth.split(' ')[0].strip()) + ' ' + str(date) + ', ' + str(getmonth.split(' ')[1].strip())

		original_html_table = schedule_df_to_html(copy.deepcopy(schedule_tables_dict[int(dateID)]))
		html_table = schedule_df_to_html(copy.deepcopy(proposed_tables_dict[int(dateID)]))
		plus_minus = str(int(plus_minus_list[int(dateID)]))

		original_number_of_shifts = len(schedule_tables_dict[int(dateID)].drop(columns=["Time", "PPH score", "Overall score"]).columns)
		proposed_number_of_shifts = len(proposed_tables_dict[int(dateID)].drop(columns=["Time", "PPH score", "Overall score"]).columns)

		PPH_value = int(round(global_pph_list[int(dateID)]))
		PI_value = int(round(global_pi_list[int(dateID)]))

		# display information
		return render_template('optimized_BB_analysis.html', current_date = current_date, original_html_table=original_html_table, html_table = html_table, plus_minus = plus_minus,
			original_number_of_shifts = original_number_of_shifts, proposed_number_of_shifts=proposed_number_of_shifts,
			PPH_value=PPH_value, PI_value=PI_value)
	return render_template('optimized_BB_schedule.html')

@app.route('/optimized_summary', methods=['GET'])
def optimized_summary():
	"""Display optimized summary"""
	global backup_shifts
	global weekends
	global day_shifts
	global night_shifts
	global total_hours

	summary_table = pd.DataFrame(index=(list(backup_shifts.keys()).sort()))
	summary_table['# Backup Shifts'] = pd.Series(backup_shifts)
	summary_table['# Day Shifts'] = pd.Series(day_shifts)
	summary_table['# Night Shifts'] = pd.Series(night_shifts)
	summary_table['# Weekend Shifts'] = pd.Series(weekends)
	summary_table['# Hours Worked'] = pd.Series(total_hours)

	total = [sum(backup_shifts.values()), sum(day_shifts.values()), sum(night_shifts.values()), sum(weekends.values()), sum(total_hours.values())]
	summary_table.loc['Total'] = total

	html_summary_table = summary_table.to_html(index=True)
	original_string = """<table border="1" class="dataframe">"""
	new_string = """<table style="width: 100%; table-layout:fixed;" class="table table-bordered table-sm table-hover">"""
	html_summary_table = html_summary_table.replace(original_string, new_string)

	for lastname, fullname in zip(list_of_last_names, list_of_doctors_total_csv):
		html_summary_table = html_summary_table.replace(f'<th>{lastname}</th>', f'<th>{fullname}</th>')

	return render_template('optimized_summary.html', summary_table=html_summary_table)





#IMPORTING AND OPTIMIZING SCHEDULES FROM BYTEBLOC 

@app.route('/optimized_calendar', methods=['GET'])
def optimize_calendar():
	global proposed_tables_dict
	global global_search_date
	global global_search_date_end
	global BBavailable_doctors
	global BBavailable_shifts
	global plus_minus_list
	global global_pph_list
	global global_pi_list
	global list_days_index_pph_minus_pi_ascending_order

	proposed_tables_dict.clear()
	background_color = []
	key = 'Emergence'
	counter = 0

	#Configuring Calendar and Variables
	schedule_start = global_search_date
	schedule_end = global_search_date_end

	calendar_dict, all_calendar_days, html_calendars = get_calendars(schedule_start, schedule_end, months)
	proposed_tables_dict = create_schedule_df_dict(all_calendar_days)
	doctor_shifts = {item.ShiftName: [item.StartTime, item.EndTime] for item in Permanent_Shifts.query.all()}
	all_ids = [x for x in range(0, len(all_calendar_days))]

	available_doctors = list(set([x.split(',')[0].strip().lower() for x in BBavailable_doctors]))
	available_shifts = doctor_shifts.keys()
	all_add_on_shifts = X_night + surges + helper_shifts

	pph_sum_list = [0 for x in range(0, len(all_calendar_days))]
	pi_sum_list = [0 for x in range(0, len(all_calendar_days))]

	print('Preparing Backup Shifts...')
	available_admin = [name for name in available_doctors if (Physician.query.filter_by(username=name).first()).admin == True]

	print('Retrieving Restrictions...')																										#Restrictions
	max_hours = {name: (Restrictions.query.filter_by(DoctorName=name).first()).TotalHours for name in available_doctors}                    #Max Total Hours Per 10 Week
	max_limit = {name: (Restrictions.query.filter_by(DoctorName=name).first()).HoursPerWeek for name in available_doctors}					#Max Number Of Hours
	max_backup_shifts = {name: (Restrictions.query.filter_by(DoctorName=name).first()).NumberBackupShifts for name in available_doctors}	#Max Number Backup Shifts
	max_weekends = {name: (Restrictions.query.filter_by(DoctorName=name).first()).NumberWeekends for name in available_doctors}				#Max Number Weekends
	max_night_shifts = {name: (Restrictions.query.filter_by(DoctorName=name).first()).NumberNightShifts for name in available_doctors}		#Max Number Night Shifts
	max_day_shifts = {name: (Restrictions.query.filter_by(DoctorName=name).first()).NumberDayShifts for name in available_doctors}			#Max Number Day Shifts
	unavailable_hours = {name: (Unavailable_Hours.query.filter_by(DoctorName=name).all()) for name in available_doctors}					#Unavailable Hours

	global backup_shifts
	global weekends
	global night_shifts
	global day_shifts
	global total_hours	

	print('Creating Restriction Trackers...')								#Restrictions Trackers
	backup_shifts = {name: 0 for name in available_doctors}					#Track number of backup shifts
	weekends = {name: 0 for name in available_doctors}						#Track number of weekends
	night_shifts = {name: 0 for name in available_doctors}					#Track number of night shifts
	day_shifts = {name: 0 for name in available_doctors}					#Track number of day shifts
	doctor_workload = {name: 0 for name in available_doctors}				#Track hours worked per week
	total_hours = {name: 0 for name in available_doctors} 					#Check total hours worked by physician
	
	#TODO-->Rearrange all_calendar_days from lowest PPH to highest
	for day, today_index in enumerate(list_days_index_pph_minus_pi_ascending_order):
		print(day)
		date = all_calendar_days[today_index]
		if today_index != 0 and today_index != (len(all_calendar_days) - 1):
			previous_day_schedule = proposed_tables_dict[today_index - 1]
			next_day_schedule = proposed_tables_dict[today_index + 1]
		elif today_index == 0:
			previous_day_schedule = None
			next_day_schedule = proposed_tables_dict[today_index + 1]
		elif today_index == (len(all_calendar_days) - 1):
			previous_day_schedule = proposed_tables_dict[today_index - 1]
			next_day_schedule = None

		#BB_shifts = [shift for shift in BBavailable_shifts[day] if shift not in ('Admin on call', 'Backup day', 'Backup night')]
		#all_add_on_shifts = [x for x in all_add_on_shifts if x not in BB_shifts]
		og_dict = update_list_of_available_doctors_with_unavailable_hours(previous_day_schedule, next_day_schedule, doctor_workload, total_hours, available_shifts, available_doctors, shift_restrictions_y, shift_restrictions_t, doctor_shifts, day_shifts, night_shifts, weekends, shift_types, backup_shifts, max_limit, max_hours, max_backup_shifts, max_day_shifts, max_night_shifts, max_weekends, date, unavailable_hours)

		try:
			day_schedule, pph_sum, pi_sum, random_doctor_row = create_basic_doctor_rows(date, og_dict, permanent_shifts_PPH, all_add_on_shifts, list_of_last_names, weekends, backup_shifts, night_shifts, day_shifts, shift_types, doctor_workload, total_hours, doctor_shifts, list_of_doctors_total_csv, model, model1, time_index)
			#day_schedule, pph_sum, pi_sum = optimize_BB_schedule(date, og_dict, BB_shifts, list_of_last_names, backup_shifts, weekends, night_shifts, day_shifts, shift_types, doctor_workload, doctor_shifts, list_of_doctors_total_csv, model, model1, time_index)
			day_schedule = add_backup_shifts(date, day_schedule, random_doctor_row, available_admin, list_of_doctors_total_csv, list_of_last_names, og_dict, doctor_shifts, backup_shifts, weekends, Restrictions)
		except IndexError:
			return render_template("error_message.html", error_message="The restrictions you entered are too strict.")
			# day_schedule = schedule_tables_dict[today_index]


		if counter % 7 == 0:
			doctor_workload = reinitialize_doctor_workload(doctor_workload)

		pph_sum_list[today_index] += pph_sum
		pi_sum_list[today_index] += pi_sum

		if pph_sum >= pi_sum: background_color.append('#19D529')				#Green
		elif pph_sum >= (0.8 * pi_sum): background_color.append('#FFD400')		#Yellow
		else: background_color.append('#FF4900')								#Red

		proposed_tables_dict[day] = day_schedule
		counter += 1

	'''
	x_axis = [x for x in range(len(pph_sum_list))]
	plt.plot(x_axis, pph_sum_list, label="PPH")
	plt.plot(x_axis, pi_sum_list, label="PI")
	plt.legend()
<<<<<<< HEAD
=======
	plt.savefig('WEIGHTED_PPH_vs_PI.png', dpi=500)
>>>>>>> 0a484320949f8b4be9312f1ad77c98c72f0956d9
	plt.savefig("Optimized_schedule_pph_and_pi_values.png", dpi=500)
	plt.close()
	'''

	#Getting Plus Minus
	new_plus_minus_list = []
	for OPPH, NPPH in zip(plus_minus_list, pph_sum_list):
		new_plus_minus_list.append(round(NPPH - OPPH))

	patients_seen = int(round(sum(pph_sum_list)))
	plus_minus = int(round(sum(new_plus_minus_list)))
	global_pph_list = pph_sum_list
	global_pi_list = pi_sum_list
	plus_minus_list = new_plus_minus_list

	#Configuring calendar
	originals, replacements = configure_calendars(calendar_dict, all_ids, all_calendar_days, background_color, weekdays)
	html_calendars = configure_html_calendars(originals, replacements, html_calendars)
	carousel_html_calendars = add_carousel_to_html_calendar_table(html_calendars)

	return render_template('optimized_BB_schedule.html', calendar = carousel_html_calendars, patients_seen=patients_seen, total_plus_minus = plus_minus, key = key)


@app.route('/secondary_optimized_calendar', methods=["GET"])
def secondary_optimize_calendar():
	"""Secondary function to optimize calednar -- work still in progress"""
	global proposed_tables_dict
	global global_search_date
	global global_search_date_end
	global BBavailable_doctors
	global BBavailable_shifts
	global plus_minus_list
	global list_days_index_pph_minus_pi_ascending_order

	global pph_sum_list
	global pi_sum_list

	proposed_tables_dict.clear()
	background_color = []
	key = 'Emergence'
	counter = 0

	#Configuring Calendar and Variables
	schedule_start = global_search_date
	schedule_end = global_search_date_end

	calendar_dict, all_calendar_days, html_calendars = get_calendars(schedule_start, schedule_end, months)
	proposed_tables_dict = create_schedule_df_dict(all_calendar_days)
	doctor_shifts = {item.ShiftName: [item.StartTime, item.EndTime] for item in Permanent_Shifts.query.all()}
	all_ids = [x for x in range(0, len(all_calendar_days))]

	available_doctors = list(set([x.split(',')[0].strip().lower() for x in BBavailable_doctors]))
	available_shifts = doctor_shifts.keys()
	all_add_on_shifts = X_night + surges + helper_shifts	


#PERSONAL DOCTOR ACCOUNTS

#DoctorHome
@app.route('/doctor_home', methods=['GET'])
def doctor_home():
	"""Render HTML for doctor page"""
	global username
	global login_parameters
	image_file = (Physician.query.filter_by(username=username).first()).image_file
	fullname = list_of_doctors_total_csv[list_of_last_names.index(username)]
	today = login_parameters[0]
	shift_date = login_parameters[1]
	SHIFT_ONE = login_parameters[2]
	SHIFT_TWO = login_parameters[3]
	start_time_one = login_parameters[4]
	end_time_one = login_parameters[5]
	start_time_two = login_parameters[6]
	end_time_two = login_parameters[7]
	return render_template('doctor_home.html', doctor_name=fullname, image_file=image_file, date_one=today, date_two=shift_date, one=SHIFT_ONE, two=SHIFT_TWO, sto=start_time_one, eto=end_time_one, stt=start_time_two, ett=end_time_two)

@app.route('/settings', methods=['GET', 'POST'])
def settings():
	"""Render HTML for doctor settings"""

	# Get doctor username
	global username
	fullname = list_of_doctors_total_csv[list_of_last_names.index(username)]
	physician = Physician.query.filter_by(username=username).first()

	if request.method == 'POST':

		# change emails (default to identical)
		new_password = request.form['password']
		new_email = request.form['email']

		if 'file' not in request.files:
			#ADD CHANGES TO PHYSICIAN DATABASE
			physician.password = new_password
			physician.email = new_email
			db.session.commit()

		else:
			image_file = request.files['file']

			# if image filename is empty string
			if image_file.filename == '':
				physician.password = new_password
				physician.email = new_email
				db.session.commit()

			# if image is allowed
			elif image_file and allowed_file(image_file.filename, ALLOWED_EXTENSIONS):
				filename = secure_filename(image_file.filename)
				image_file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
				physician.password = new_password
				physician.email = new_email
				physician.image_file = str(filename)
				db.session.commit()

	password = physician.password
	email = physician.email
	image_file = physician.image_file
	return render_template('settings.html', doctor_name=fullname, password=password, email=email, image_file=image_file)

#Personal Schedule
@app.route('/personal_schedule', methods=['GET'])
def get_personal_schedule():
	"""Display personal schedule"""

	global username		# doctor username
	weekly_schedule_dict = {}
	html_weekly_schedule = []
	dates = []

	doctor_shifts = {item.ShiftName: [item.StartTime, item.EndTime] for item in Permanent_Shifts.query.all()}	# get doctor_shift: shift_times
	all_shifts = Shifts.query.filter_by(DoctorName=username.split(',')[0].strip().lower()).all()	# get shifts for this doctor
	today = datetime.datetime.strptime('2017-04-10', "%Y-%m-%d")
	shift_dates = [datetime.datetime.strptime(x.Date, "%Y-%m-%d") for x in all_shifts]
	shift_names = [x.ShiftName for x in all_shifts]
	shifts_dates_AT, shift_names_AT = get_dates_after_today(shift_dates, shift_names, today)
	
	monday1 = (shifts_dates_AT[0] - datetime.timedelta(days=shifts_dates_AT[0].weekday()))
	monday2 = (shifts_dates_AT[-1] - datetime.timedelta(days=shifts_dates_AT[-1].weekday()))
	num_of_weeks = (monday2 - monday1).days // 7
	
	for i in range(0, num_of_weeks):
		current_monday = monday1 + datetime.timedelta(days=(i * 7))
		date_key = str(current_monday).split(' ')[0].strip()
		dates.append(date_key)
		#print(current_monday, current_monday.weekday())
		weekly_schedule = pd.DataFrame(index=range(24), columns=complete_weekdays)

		for index, shift in enumerate(shifts_dates_AT):
			if shift - datetime.timedelta(days=shift.weekday()) == current_monday:
				weekday = complete_weekdays[shift.weekday()]
				name = shift_names_AT[index]
				start_time = doctor_shifts[name][0] - 7
				end_time = doctor_shifts[name][1] - 7

				for hour in range(start_time, end_time):
					weekly_schedule = weekly_schedule.set_value(hour, weekday, name)

		weekly_schedule.insert(loc=0, column='Time', value=time_index)
		weekly_schedule.replace(np.nan, '', regex = True, inplace=True)
		weekly_schedule_dict[date_key] = weekly_schedule

		html_table = weekly_schedule.to_html(index=False)
		original_string = """<table border="1" class="dataframe">"""
		new_string = f"""<table style="width:80%; table-layout:fixed;" class="table table-bordered table-sm" id="{date_key}">"""
		html_table = html_table.replace(original_string, new_string)
		html_weekly_schedule.append(html_table)

	schedule_with_carousel = add_carousel_to_personal_schedule(html_weekly_schedule, dates) #ADD CAROUSEL	

	return render_template('personal_schedule.html', weekly_schedule = schedule_with_carousel, dates=dates)

#Unavailable Hours
@app.route('/set_personal_preferences', methods=['GET', 'POST'])
def preferences():
	"""Code for doctor to set personal preferences"""
	global username
	global preference_ids

	start_date = datetime.datetime.today()
	end_date = start_date + datetime.timedelta(days=365)
	
	calendar_dict, all_calendar_days, html_calendars = get_calendars(start_date, end_date, months)
	originals, replacements = configure_restrictions_calendars(calendar_dict, all_calendar_days, weekdays)
	html_calendars = configure_html_restrictions_calendars(originals, replacements, html_calendars)
	html_calendars = add_carousel_to_html_calendar_table(html_calendars)

	preferences_table = pd.DataFrame(index=range(24), columns=complete_weekdays)

	if request.method == 'POST':
		Unavailabilities = [x for x in preference_ids if request.form.get(x) == 'on']
		doctor_name = username.split(',')[0].strip().lower()

		past_restrictions = Unavailable_Hours.query.filter_by(DoctorName=doctor_name).all()
		for PR in past_restrictions:
			db.session.delete(PR)

		for UNA in Unavailabilities:
			weekday = complete_weekdays.index(UNA.split(' ')[0].strip())
			time = int(UNA.split(' ')[1]) + 7
			NR = Unavailable_Hours(DoctorName=doctor_name, Weekday=weekday, Time=time)
			db.session.add(NR)
		
		db.session.commit()

	all_unavailable_hours = Unavailable_Hours.query.filter_by(DoctorName=username.split(',')[0].strip().lower()).all()
	ids = []
	red_ids = []

	for index, hour in preferences_table.iterrows():
		for i, weekday in enumerate(hour):
			preferences_table.set_value(index, complete_weekdays[i], complete_weekdays[i] + ' ' + str(index))	
			ids.append(complete_weekdays[i] + ' ' + str(index))

			for UN in all_unavailable_hours:
				if UN.Weekday == i and int(UN.Time) - 7 == index:
					preferences_table.set_value(index, complete_weekdays[i], complete_weekdays[i] + ' ' + str(index) + ' ' + 'red')
					red_ids.append(complete_weekdays[i] + ' ' + str(index) + ' ' + 'red')

	preferences_table.insert(loc=0, column='Time', value=time_index)
	preferences_table.replace(np.nan, '', regex=True, inplace=True)

	html_table = preferences_table.to_html(index=False)
	original_string = """<table border="1" class="dataframe">"""
	new_string = """<table style="width:90%; table-layout:fixed; margin-left:auto; margin-right:auto" class="table table-bordered table-sm">"""
	html_table = html_table.replace(original_string, new_string)

	for id_ in ids:
		html_table = html_table.replace(f"""<td>{id_}</td>""", f"""<td id='{id_}'><input type="checkbox" name="{id_}"></td>""")
	for id_ in red_ids:
		html_table = html_table.replace(f"""<td>{id_}</td>""", f"""<td id='{id_}' bgcolor='#FF4900'><input type="checkbox" name="{id_}" checked></td>""")	

	preference_ids.clear()
	all_ids = ids + red_ids
	preference_ids = [x for x in all_ids]
	return render_template('my_preferences.html', preference_table=html_table, restrictions_calendar=html_calendars, all_ids=all_ids)



#EXTRA FEATURES


# Calculator display
@app.route('/calculator', methods=["GET", "POST"])
def calculator():
	"""Calculator to compute PPH values for a combination of doctors"""

	if request.method == "POST":
		try:
			# get doctor names
			list_of_doctors_current = []
			form_names = ["doctor_1", "doctor_2", "doctor_3", "doctor_4", "doctor_5", "doctor_6"]
			for form_name in form_names:
				list_of_doctors_current.append(request.form[form_name])

			# get time format
			time_military_format = request.form["hour_of_day"]
			time_military_format = int(time_military_format.split(':')[0])

			current_date = datetime.datetime.strptime(request.form["search_date"],"%Y-%m-%d")
			search_date_string = current_date.strftime("%a, %d-%b-%Y").replace(" 0", " ")

			month = search_date_string.split('-')[1]
			month_vector = monthto(months, month)

			# predict
			prediction = backend_predict(list_of_doctors_current, time_military_format, month_vector, model, list_of_doctors_total_csv)

			time_military_format = 0

			prediction_string = f"<p>The predicted PPH value is <b>{prediction[0]}</b>.</p>"
			prediction_string += "<p>The doctors working were:</p><ul>"

			for doctor_name in list_of_doctors_current:
				if doctor_name != '':
					prediction_string += f"<li>{doctor_name}</li>"
			prediction_string += "</ul>"
			prediction_string += f"""The time was <b>{request.form["hour_of_day"]}</b>.<br>"""
			prediction_string += f"The date was <b>{search_date_string}</b>."

			list_of_doctors_current = []

		# if there is an error
		except ValueError:
			prediction_string = "<h6>Sorry, we're unable to render your submission! You may have left some forms unfilled.</h6>"

		return render_template("calculator.html", list_of_doctors=list_of_doctors_total_csv, prediction=prediction_string)

	# render calculator.html without output if there is no POST request
	return render_template("calculator.html",  list_of_doctors=list_of_doctors_total_csv)

@app.route("/display", methods=["GET", "POST"])
def display():
	"""Outdated function?"""

	# get doctor names
	list_of_doctors_current = []
	form_names = ["doctor_1", "doctor_2", "doctor_3", "doctor_4", "doctor_5"]
	for form_name in form_names:
		list_of_doctors_current.append(request.form[form_name])

	# get time format
	time_military_format = request.form["hour_of_day"]
	time_military_format = int(time_military_format.split(':')[0])

	current_date = datetime.datetime.strptime(request.form["search_date"],"%Y-%m-%d")
	search_date_string = current_date.strftime("%a, %d-%b-%Y").replace(" 0", " ")

	month = search_date_string.split('-')[1]
	month_vector = monthto(months, month)

	# predict
	prediction = backend_predict(list_of_doctors_current, time_military_format, month_vector, model, list_of_doctors_total_csv)

	list_of_doctors_current = []
	time_military_format = 0

	return render_template("display.html", prediction=prediction[0])

# Feedback code

@app.route("/feedback")
def feedback():
	"""Template for doctor feedback"""
	#Render HTML template for doctor Feedback
	global username
	doctor_shifts = {item.ShiftName: [item.StartTime, item.EndTime] for item in Permanent_Shifts.query.all()}
	today = datetime.datetime.strptime('2017-04-10', "%Y-%m-%d")
	all_shifts = Shifts.query.filter_by(DoctorName=username.split(',')[0].strip().lower()).all()
	shift_dates = [datetime.datetime.strptime(x.Date, "%Y-%m-%d") for x in all_shifts]
	shift_names = [x.ShiftName for x in all_shifts]
	closest_date, date_index = nearest_date(shift_dates, today)
	input_date = str(closest_date).split(' ')[0].strip()
	current_shift = shift_names[date_index]
	start_time = doctor_shifts[current_shift][0]
	end_time = doctor_shifts[current_shift][1]
	return render_template("input_feedback.html", user=username, shift=current_shift, date=input_date, start_time=start_time, end_time=end_time)


@app.route("/compute_feedback", methods=["GET", "POST"])
def compute_feedback():
	"""Save doctor feedback in CSV format"""
	if request.method == 'POST':

		# read contents from feedback() function
		doctor_name = list_of_doctors_total_csv[list_of_last_names.index(request.form["doctor_name"])]
		shift_name = request.form["shift_name"]
		date = request.form["day"]
		submit_time = request.form["shift_time"]
		satisfaction = request.form["satisfaction"]
		stress = request.form["stress"]
		tired = request.form["tired"]
		comment = request.form["comment"]

		csv_row = [doctor_name, shift_name, date, submit_time, satisfaction, stress, tired, comment]
		with open('dataset/feedback.csv', 'a') as f:
			writer = csv.writer(f)
			writer.writerow(csv_row)

		return render_template("success_message.html", doctor_name=doctor_name)

	return render_template("error_message.html", error_message="We didn't receive a valid form input. Please try entering the feedback form again.")

@app.route("/download", methods=["GET", "POST"])
def download():
	"""Downloads CSV file for proposed schedule"""

	# list of PPH-PI for each day based on index
	global plus_minus_list
	csv_string = ""		# output string saved to the CSV file

	current_date = global_search_date
	start_date = current_date.strftime('%m/%d/%Y')

	# for day_index: pandas table of schedule in dictionary of proposed schedules
	for index, raw_table in proposed_tables_dict.items():
		csv_string += "\n"
		csv_string += current_date.strftime('%m/%d/%Y')
		csv_string += ", patient difference: " + str(plus_minus_list[index])
		csv_string += "\n"
		header_string = "Shift, Doctor\n"
		csv_string += header_string

		# remove columns with no doctor names
		modified_table = raw_table.drop(columns=["Time",  'PPH score', 'Overall score'])
		current_shifts = modified_table.columns.values

		for shift_name in current_shifts:
			doctor_name = np.unique(modified_table[shift_name].tolist())
			doctor_name = [x for x in doctor_name if x]
			shift_string = '\"'+ shift_name + '\",\"' + doctor_name[0] + '\" \n'
			csv_string += shift_string

		csv_string += '\n'

		end_date = current_date.strftime('%m/%d/%Y')
		current_date = current_date + datetime.timedelta(days=1)

	csv_file_header = f"Recommended Schedule from {start_date} to {end_date}\nMore patients seen: {sum(plus_minus_list)}\n\n"
	csv_string = csv_file_header + csv_string

	# downloads the CSV file
	resp = make_response(csv_string)
	resp.headers["Content-Disposition"] = "attachment; filename=export.csv"
	resp.headers["Content-Type"] = "text/csv"
	return resp


if __name__ == '__main__':
    app.run(debug=True)
