"""
Computing the waiting time for each doctor
"""

import pandas as pd 
from datetime import datetime
import numpy as np

df = pd.read_excel("dataset/Copy of Sample Data(172).xlsx", sheet_name="Sheet1")

list_of_unique_doctor_names = df["StaffMD"].unique().tolist()
print(list_of_unique_doctor_names)

dict_of_doctors__waiting_time_list = {doctor_name:[] for doctor_name in list_of_unique_doctor_names}

verbose = False

list_of_waiting_times = []

for index, row in df.iterrows():
	

	NPIA_string = row["PIA_NPIA"]
	Arrival_string = row["Arrival"]

	# print(NPIA_string)
	# print(Arrival_string)

	diff = NPIA_string - Arrival_string

	if verbose:
		print(row["Arrival"], end='\t')
		print(row["PIA_NPIA"], end='\t')
		print(row["StaffMD"], end='\t')
		print(int(diff.total_seconds()))

	temp_row = []
	temp_row.append(row["Arrival"])
	temp_row.append(row["PIA_NPIA"])
	temp_row.append(row["StaffMD"])
	temp_row.append(int(diff.total_seconds()))

	list_of_waiting_times.append(temp_row)

np.savetxt("dataset_of_waiting_times.csv", list_of_waiting_times, delimiter=',', fmt="%s")


for index, row in df.iterrows():

	