# Emergence

Using Hospital Data to Improve Scheduling and Efficiency

Add ons/problems/ideas: If you don't know what to do, you can choose a topic here and try to develop it.
1. Build model that predicts CTAS score instead of PPH. CTAS can be inverted and summed up to create labels and thresholds can be defined per hour instead of per shift.
2. Build generator model that generates an optimized team of doctors for a specific day in a specific month. (Generative Adversarial Network or Trial and error)
3. Create feature analysis method that detects the doctors on a shift who are bringing down the overall PPH.
4. Design method to evaluate the overall score of an entire day (between 0 and 1) that can be used to discriminate the generator model.
5. Design calendar instead of dropdown to better visualize the schedule. 
6. Find a way to compute a confidence score when using a linear regression model. (There isn't a build in method with keras.)

If you have any new ideas, feel free to add them to the list.

CURRENT ERRORS:
* schedule upload: 'Night shift backup' not contained in axis
* Doctor account: bug with "preferences" in menu


UPDATE JANUARY 27, 2019:
* Update with PIA instead PPH metric (Stephen)
* Number of shifts to add/remove based on PIA (Laurence)
* Overstaffing hours (Laurence) > overall
* Visualize using line chart (Laurence)
* Add functionality to customize the modifying database (if we have time)