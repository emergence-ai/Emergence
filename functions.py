"""
Ensemble of functions used by main.py
"""

# Import modules
import pandas as pd 
import glob
import os

import random
import collections
import numpy as np
import pandas as pd
import string
import csv
import json
import datetime
import math
import pickle
import operator
import calendar
import itertools

def daterange(date1, date2):
	"""Yield datetime objects"""
	from datetime import timedelta, date
	for n in range(int ((date2 - date1).days)+1):
		yield date1 + timedelta(n)

def allowed_file(filename, ALLOWED_EXTENSIONS):
	"""Check whether image is in valid format
	Return"""
	return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def process_weights(vector, model, doctor_list):
	"""
	Takes into input the vector for the row and the list of doctor full names

	Returns dictionary of { doctor_name : weight }
	The higher the weight, the more important the doctor is to making the team more efficient
	"""

	number_of_doctors = len(doctor_list)

	doctor_vector = vector[:number_of_doctors]  # vector of doctors
	date_vector = vector[number_of_doctors:]    # vector of hour and date
	overall_number_of_doctors_in_selection = len([i for i, x in enumerate(doctor_vector) if x == 1])

	# overall PPH score
	overall_score = model.predict(np.array([vector]))[0]

	# dictionary with the doctor name key associated to the weight value
	doctor_weights = {}

	# get doctor indices
	doctor_indices = [i for i, x in enumerate(doctor_vector) if x == 1]

	for doctor_index in doctor_indices:

		# get doctor's full name
		doctor_full_name = doctor_list[doctor_index]

		# remove the specified doctor from the list (1 becomes 0)
		temp_doctor_vector = doctor_vector.copy()
		temp_doctor_vector[doctor_index] = 0
		combined_vector = temp_doctor_vector + date_vector

		# compute weight
		prediction = model.predict(np.array([combined_vector]))[0]
		number_of_doctors_in_selection = len([i for i, x in enumerate(temp_doctor_vector) if x == 1])
		weight = overall_score/overall_number_of_doctors_in_selection - prediction/number_of_doctors_in_selection

		# assign key and value to dictionary
		doctor_weights[doctor_full_name] = weight

	return doctor_weights

def vectorize_doctors(list_of_doctors_current, list_of_doctors_total):
	"""
	Return one-hot vector of combination of doctors working at a specific time
	
	list_of_doctors_current is list of doctors currently working
	list_of_doctors_total is list of total doctors
	"""

	doctor_vector = [0] * len(list_of_doctors_total)
	for doctor_name_current in list_of_doctors_current:
		for doctor_name_total in list_of_doctors_total:
			if doctor_name_current == doctor_name_total:
				doctor_vector[list_of_doctors_total.index(doctor_name_current)] = 1

	return doctor_vector

def get_BB_doctors_and_shifts(df_schedule, list_of_last_names, list_of_doctors_total_csv, BBavailable_doctors, BBavailable_shifts, search_date, all_calendar_days):
	"""
	Return list of available doctors and available shifts
	"""

	shift_names = df_schedule.columns	# names of shifts

	# for day, day in all calendar days:
	for day, date in enumerate(all_calendar_days):

		# create valid search string
		search_date_string = (search_date + datetime.timedelta(day)).strftime("%a, %d-%b-%Y").replace(" 0", " ")
		
		# find row corresponding to date search string
		temp_row = df_schedule.ix[search_date_string]
		available_shifts = []
		available_doctors = []

		# for shift_index, doctor in the row
		for shift, doc in enumerate(temp_row):

			# for index, doctor last name in the list of last names:
			for ind, lastname in enumerate(list_of_last_names):

				# if the names are the same
				if doc.lower().strip() == lastname.lower().strip():

					available_shifts.append(shift_names[shift]) 				# add shift to available shifts
					available_doctors.append(list_of_doctors_total_csv[ind])	# add doctor to available doctors
		BBavailable_shifts.append(available_shifts)
		BBavailable_doctors.extend(available_doctors)
	BBavailable_doctors = list(set(BBavailable_doctors))		# unique list of available doctors
	return BBavailable_doctors, BBavailable_shifts

def fill_BB_day_schedule(schedule_col_names, temp_row, list_of_last_names, doctor_shifts, list_of_doctors_total_csv, day_schedule):
	"""
	Return day schedule, doctors working and used shifts

	day_schedule: empty Pandas dataframe schedule (24 rows)
	"""
	doctors = []
	shifts = []

	# for column name and row :
	for column, value in zip(schedule_col_names, temp_row):

		# for each doctor last name
		for ind, lastname in enumerate(list_of_last_names):

			# if they correspond
			if value.lower().strip() == lastname.lower().strip():

				# link range based on doctor shifts
				list_range_hours = doctor_shifts[column]
				shifts.append(column)

				# substract shift hours by 7
				start = list_range_hours[0] - 7
				end = list_range_hours[1] - 7

				# get doctor full name based on index position of last name
				doctor_full_name_current = list_of_doctors_total_csv[ind]
				doctors.append(doctor_full_name_current)
				for hour in range(start, end):
					day_schedule = day_schedule.set_value(hour, column, doctor_full_name_current)   

	return day_schedule, doctors, shifts

def fill_day_schedule(schedule_col_names, temp_row, list_of_last_names, doctor_shifts, list_of_doctors_total_csv, day_schedule):
	"""
	Fill day schedule with column names, list of last names, and doctor shifts
	"""
	for column, value in zip(schedule_col_names, temp_row):
		for ind, lastname in enumerate(list_of_last_names):
			if value.lower() == lastname.lower():
				list_range_hours = doctor_shifts[column]
				start = list_range_hours[0] - 7
				end = list_range_hours[1] - 7
				doctor_full_name_current = list_of_doctors_total_csv[ind]
				for hour in range(start, end):
					day_schedule = day_schedule.set_value(hour, column, doctor_full_name_current)
	return day_schedule

def timeto(hour):
	"""One-hot of vector time"""
	time_vector = [0]*24
	time_vector[hour] = 1
	return time_vector

def weekto(weekday):
	"""One-hot of weekday"""
	week_vector = [0] * 7
	week_vector[weekday] = 1
	return week_vector

def monthto(months, month):
	"""One-hot of month"""
	month_index = months.index(month)
	month_vec = [0] * 12
	month_vec[month_index] = 1
	return month_vec	 

def backend_predict(list_of_doctors_current, time_military_format, month_vector, model, list_of_doctors_total):
	"""
	TODO update with weekday implementation
	Used for the PPH calculator
	Predict PPH value based on list of doctors, time, and month
	"""
	doctor_vector = vectorize_doctors(list_of_doctors_current=list_of_doctors_current, list_of_doctors_total=list_of_doctors_total)
	time_vector = timeto(time_military_format)
	combined_vector = doctor_vector + time_vector + month_vector
	combined_vector = np.array([combined_vector])
	prediction = model.predict(np.array(combined_vector))
	return prediction

def calculate_overall_score_with_PIP(pph_list, pi_list, green_threshold=0.95, red_threshold=0.80, blue_threshold=1.2):
	"""
	Returns list with color scoring based on threshould
	""" 

	Overall = []	# list containing color codes
	counter = range(len(pph_list))
	for pph, pi, count in zip(pph_list, pi_list, counter):

		# if PPH is over 0.95*PI values, then append green code
		if blue_threshold*pi > pph >= (green_threshold * pi): Overall.append('Green')

		# if there is overstaffing
		elif pph >= blue_threshold*pi: Overall.append("Blue")

		# elif PPH is smaller than PI, but is during the last 3 hours, then append orange color code
		elif pph < pi and count == 21 or pph < pi and count == 22 or pph < pi and count == 23: Overall.append('Orange')

		# e;if PPH is superior than PI between 0.80 and 0.95, then append yellow code
		elif pph >= (red_threshold * pi): Overall.append('Yellow')

		# otherwise if PPH is inferior than 0.80*PI, then append red code
		else: Overall.append('Red')

	return Overall

def vectorize_row_with_PIP(date, day_schedule, month_vec, model, model1, list_of_doctors_total_csv):
	"""
	Returns PPH list and PI list based on day_schedule and PPH and PI predictions

	model: PPH predictor
	model1: PI predictor
	"""
	pph_list = []
	pi_list = []
	weekday_vec = weekto(datetime.datetime.strptime(date, "%Y-%m-%d").weekday() - 1)
	for index, row in day_schedule.iterrows():
		list_of_doctors_per_row = []
		for i, doctor in enumerate(row):
			if pd.isnull(doctor) == False:
				list_of_doctors_per_row.append(doctor)

		# vectorize (one-hot) input data
		vectorized_row_doctors = vectorize_doctors(list_of_doctors_per_row, list_of_doctors_total_csv)
		vectorized_hour = timeto((index + 7) % 24)
		vector1 = vectorized_row_doctors + vectorized_hour + month_vec
		vector2 = vectorized_hour + month_vec + weekday_vec

		# make prediction
		PPH_prediction = model.predict(np.array([vector1]))		# PPH
		PI_prediction = model1.predict(np.array([vector2]))		# PI
		pph_list.append(PPH_prediction)
		pi_list.append(PI_prediction)

	pph_list = np.array(pph_list).flatten()
	pph_list = [round(value, 1) for value in pph_list]
	pi_list = np.array(pi_list).flatten()
	pi_list = [round(value, 1) for value in pi_list]

	return pph_list, pi_list


def get_calendars(search_date ,search_date_end, months):
	"""Creates all HTML calendars"""

	#Getting calendar months

	calendar_dict = {}
	calendar_months = []
	all_calendar_days = []
	html_calendars = []

	for dt in daterange(search_date, search_date_end):
		calendar_months.append(dt.strftime("%Y-%m"))
		all_calendar_days.append(dt.strftime("%Y-%m-%d"))

	unique_months = sorted(list(set(calendar_months)))
	for var in unique_months:
		m = months[int(var.split('-')[1].strip()) - 1] + var.split('-')[0].strip()
		calendar_dict[m] = calendar_months.count(var)
				
	for date in unique_months:
		year = int(date.split('-')[0].strip())
		month = int(date.split('-')[1].strip())
		raw = calendar.HTMLCalendar(calendar.SUNDAY)
		raw_calendar = raw.formatmonth(year, month)
		html_calendars.append(raw_calendar)

	return calendar_dict, all_calendar_days, html_calendars

def create_schedule_df_dict(all_calendar_days):
	"""
	Create dictionary of day_index: pandas_dataframe for day schedule
	"""
	schedule_dict = {}
	for day, date in enumerate(all_calendar_days):
		schedule_dict[day] = None
	return schedule_dict	

def configure_restrictions_calendars(calendar_dict, all_calendar_days, weekdays):
	originals = []
	replacements = []

	for month, length in calendar_dict.items():
		original_days = []
		new_days = []

		important_days = all_calendar_days[:length]

		for index, date in enumerate(important_days):
			y, m, day = (int(x) for x in date.split('-')) 
			weekday = weekdays[datetime.date(y, m, day).weekday()]

			original_days.append(f"""<td class="{weekday}">{day}</td>""")
			new_days.append(f"""<td class="{weekday}">{day}<input type="checkbox" name="{date}"></td>""")

		originals.append(original_days)
		replacements.append(new_days)

		all_calendar_days = all_calendar_days[length:]

	return originals, replacements

def configure_html_restrictions_calendars(originals, replacements, html_calendars):
	for index, cal in enumerate(html_calendars):
		original_string = """<table border="0" cellpadding="0" cellspacing="0" class="month">"""
		new_string = """<table style="width=90%; table-layout:fixed;" margin-left:auto; margin-right:auto class="table table-bordered table-sm">"""
		cal = cal.replace(original_string, new_string)

		orig = originals[index]
		repl = replacements[index]

		for i, o in enumerate(orig):
			r = repl[i]
			cal = cal.replace(o, r)

		html_calendars[index] = cal	
	return html_calendars	

def configure_calendars(calendar_dict, all_ids, all_calendar_days, background_color, weekdays):
	"""
	Generate color calendar
	"""
	originals = []
	replacements = []

	for month, length in calendar_dict.items():
		original_days = []
		new_days = []

		important_ids = all_ids[:length]
		important_days = all_calendar_days[:length]
		important_backgrounds = background_color[:length]

		for index, date in enumerate(important_days):
			y, m, day = (int(x) for x in date.split('-')) 
			weekday = weekdays[datetime.date(y, m, day).weekday()]
			bgcolor = important_backgrounds[index]
			id_ = important_ids[index]

			original_days.append(f"""<td class="{weekday}">{day}</td>""")
			new_days.append(f"""<td id="{id_}" bgcolor="{bgcolor}" class="{weekday}"><button class="table" type="button">{day}</button></td>""")

		originals.append(original_days)
		replacements.append(new_days)

		all_calendar_days = all_calendar_days[length:]
		background_color = background_color[length:]
		all_ids = all_ids[length:]

	return originals, replacements		

def configure_html_calendars(originals, replacements, html_calendars):
	"""Generate HTML calendar
	Relies on configure_calendars()"""
	for index, cal in enumerate(html_calendars):
		original_string = """<table border="0" cellpadding="0" cellspacing="0" class="month">"""
		new_string = """<table style="width=100%; table-layout:fixed;" class="table table-bordered">"""
		cal = cal.replace(original_string, new_string)

		orig = originals[index]
		repl = replacements[index]

		for i, o in enumerate(orig):
			r = repl[i]
			cal = cal.replace(o, r)

		html_calendars[index] = cal	
	return html_calendars	

def add_carousel_to_html_calendar_table(raw_html_table_list):
	"""This function adds Bootstrap 4 carousel to calendars"""
	count = 0
	html_table_with_carousel = ''
	html_table_with_carousel += """<div id="carouselExampleControls" class="carousel slide" data-ride="carousel"> <div class="carousel-inner">"""
	for item in raw_html_table_list:
		if count == 0:
			temp = """<div class="carousel-item active">""" + item 
		else:
			temp = """<div class="carousel-item">""" + item
		temp = temp + "</div>"
		html_table_with_carousel += temp
		count += 1
	html_table_with_carousel += """</div>
  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>"""
	return html_table_with_carousel

def add_carousel_to_personal_schedule(schedule, dates):
	"""This function adds Bootstrap 4 caroulsel to personal schedules"""
	count = 0
	html_table_with_carousel = ''
	html_table_with_carousel += """<div id="carouselExampleControls" class="carousel slide" data-ride="carousel"> <div class="carousel-inner">"""
	for index, item in enumerate(schedule):
		if count == 0: temp = f"""<div class="carousel-item active"><h4 style="font-family: 'Trebuchet MS', Helvetica, sans-serif; text-align: center;">{dates[index]}</h4>""" + item
		else: temp =  f"""<div class="carousel-item"><h4 style="font-family: 'Trebuchet MS', Helvetica, sans-serif; text-align: center;">{dates[index]}</h4>""" + item
		temp = temp + "</div>"
		html_table_with_carousel += temp
		count += 1
	html_table_with_carousel += """</div>
  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>"""
	return html_table_with_carousel	

def feature_importance(list_of_doctors_total_csv, raw_table, month_vector, model):
	"""Color code text"""
	for index, row in raw_table.iterrows():
		#Time Vector
		time_vector = timeto((index+7) % 24)

		# remove columns that aren't needed
		doctor_row = row.drop(labels=['Time', 'PPH score', 'Overall score', 'Admin on call', 'Backup day', 'Backup night'])
		doctor_vector = [0] * len(list_of_doctors_total_csv)
		for doctor_name in doctor_row:
			doctor_last_name = doctor_name.split(',')[0]
			if any(doctor_last_name.lower() == doctor_full_name.lower().split(',')[0] for doctor_full_name in list_of_doctors_total_csv):
				doctor_index = list_of_doctors_total_csv.index(doctor_name)
				doctor_vector[doctor_index] = 1

		#If number of doctors in row is equal to 0
		if doctor_vector.count(1) == 0:
			new_doctor_name = 'No doctor registered'
			new_row = row.replace(doctor_name, new_doctor_name)
			raw_table.ix[index] = new_row

		#If number of doctors in row is equal to 1
		elif doctor_vector.count(1) == 1 and row['Overall score'] == 'Orange':
			new_doctor_name = '!' + doctor_name +'!!'
			new_row = row.replace(doctor_name, new_doctor_name)
			raw_table.ix[index] = new_row
		
		#If number of doctors is superior to 1	
		elif row['Overall score'] == 'Red':
			input_vector = doctor_vector + time_vector + month_vector
			doctor_weights = process_weights(input_vector, model, list_of_doctors_total_csv)

			for doctor, weight in doctor_weights.items():
				if weight > 0:
					new_doctor_name = '!' + str(doctor) + '!!'
					row.replace(doctor, new_doctor_name, inplace=True)

			highest = max(doctor_weights.items(), key=operator.itemgetter(1))[0]
			new_doctor_name = '!' + str(highest) +'!!'
			row.replace(highest, new_doctor_name, inplace=True)

			raw_table.ix[index] = row

		elif row['Overall score'] == 'Yellow':
			input_vector = doctor_vector + time_vector + month_vector
			doctor_weights = process_weights(input_vector, model, list_of_doctors_total_csv)
			
			for doctor, weight in doctor_weights.items():
				if weight > 0:
					new_doctor_name = '?' + str(doctor) + '??'
					row.replace(doctor, new_doctor_name, inplace=True)

			highest = max(doctor_weights.items(), key=operator.itemgetter(1))[0]
			new_doctor_name = '?' + str(highest) +'??'
			row.replace(highest, new_doctor_name, inplace=True)

			raw_table.ix[index] = row	

	return raw_table			


def add_shifts_to_database(db, Shifts, df, list_of_doctors_total_csv):
	dates = [str(x).split(' ')[0].strip() for x in df['Dates'].tolist()]
	new_df = df.drop(columns=['Dates'])
	shift_names = [str(x).strip() for x in list(new_df.columns.values)]
	for index, row in new_df.iterrows():
		for shift, name in enumerate(row):
			new_name = name.strip().lower()
			if any(new_name == doctor_name.split(',')[0].strip().lower() for doctor_name in list_of_doctors_total_csv):
				new_shift = Shifts(Date=dates[index], DoctorName=new_name, ShiftName=shift_names[shift])
				db.session.add(new_shift)
				db.session.commit()

def nearest_date(items, pivot):
    closest_date = min(items, key=lambda x: abs(x - pivot))		
    date_index = items.index(closest_date)
    return closest_date, date_index

def get_dates_after_today(shift_dates, shift_names, today):
	shift_dates_AT = []
	shift_names_AT = []
	for index, date in enumerate(shift_dates):
		if date >= today:
			shift_dates_AT.append(date)
			shift_names_AT.append(shift_names[index])

	return shift_dates_AT, shift_names_AT		

def get_doctors_and_shifts(table):
	"""Return dictionary of format shift_name:doctor_name"""
	table = table.drop(columns=['Time', 'PPH score', 'Overall score'])
	shift_names = table.columns.values
	doctor_on_shift_dict = {}
	for index, row in table.iterrows():
		for shift, doctor in enumerate(row):
			if doctor != '' and doctor not in doctor_on_shift_dict.values():
				doctor_on_shift_dict[shift_names[shift]] = doctor.split(',')[0].strip().lower()
	return doctor_on_shift_dict			

def create_new_og_dict(available_shifts):
	"""Create blank dictionary with shift:[empty but to contain doctor names] format"""
	og_dict = {}
	for shift in available_shifts:
		og_dict[shift] = []
	return og_dict

def reinitialize_doctor_workload(doctor_workload):
	new_doctor_workload = doctor_workload.fromkeys(doctor_workload, 0)
	return new_doctor_workload

def update_list_of_available_doctors_with_unavailable_hours(previous_day_schedule, next_day_schedule, doctor_workload, total_hours, available_shifts, available_doctors, shift_restrictions_y, shift_restrictions_t, doctor_shifts, day_shifts, night_shifts, weekends, shift_types, backup_shifts, max_limit, max_hours, max_backup_shifts, max_day_shifts, max_night_shifts, max_weekends, date, unavailable_hours):
	"""Restrictions function"""
	og_dict = create_new_og_dict(available_shifts)		# SHIFT: [List of available doctors]
	current_weekday = datetime.datetime.strptime(date, "%Y-%m-%d").weekday()	# convert to datetime object

	# If there is a previous day and a next day
	if previous_day_schedule is not None and next_day_schedule is not None:

		# previous day and next day doctors
		previous_day_docs = get_doctors_and_shifts(previous_day_schedule)
		next_day_docs = get_doctors_and_shifts(next_day_schedule)

		# for shift in list of available shifts
		for shift in available_shifts:

			shift_type = shift_types[shift]
			for doctor in available_doctors:

				### RESTRICTIONS ###

				# Total doctor workload
				if total_hours[doctor] + (doctor_shifts[shift][1] - doctor_shifts[shift][0]) >= max_hours[doctor] and shift not in ('Admin on call', 'Backup night', 'Backup day'): continue														#Check Total Hours
				
				# Number of hours per week
				if doctor_workload[doctor] + (doctor_shifts[shift][1] - doctor_shifts[shift][0]) >= max_limit[doctor] and shift not in ('Admin on call', 'Backup night', 'Backup day'): continue													#Check Doctor Workload
				
				# Number of day shifts per cycle
				if day_shifts[doctor] >= max_day_shifts[doctor] and shift_type == 'day': continue																				#Check Day Shifts
				
				# Number of night shifts per cycle
				if night_shifts[doctor] >= max_night_shifts[doctor] and shift_type == 'night': continue																			#Check Night Shifts
				
				# Number of backup shifts per cycle
				if backup_shifts[doctor] >= max_backup_shifts[doctor] and shift_type == 'backup': continue																		#Check Backup Shifts
				
				# Number of weekend shifts per cycle
				if weekends[doctor] >= max_weekends[doctor] and current_weekday == 5 or weekends[doctor] >= max_weekends[doctor] and current_weekday == 6: continue				#Check Weekend Shifts

				# check for doctor specific and time-specific restrictions
				restricted = False
				current_restrictions = unavailable_hours[doctor]
				for restriction in current_restrictions:
					for x in range(doctor_shifts[shift][0], doctor_shifts[shift][1] + 1):
						if restriction.Time == x and restriction.Weekday == current_weekday:
							restricted = True

				if restricted == True: 
					continue

				# doctor restrictions based on shift time
				elif doctor not in og_dict[shift]:

					if doctor not in previous_day_docs.values() and doctor not in next_day_docs.values():
						og_dict[shift].append(doctor)

					elif doctor in previous_day_docs.values() and doctor not in next_day_docs.values():	
						y_shift = [key for key, value in previous_day_docs.items() if value == doctor][0]
						if shift in shift_restrictions_y[y_shift]:
							og_dict[shift].append(doctor)

					elif doctor not in previous_day_docs.values() and doctor in next_day_docs.values():	
						t_shift = [key for key, value in next_day_docs.items() if value == doctor][0]
						if shift in shift_restrictions_t[t_shift]:
							og_dict[shift].append(doctor)

					elif doctor in previous_day_docs.values() and doctor in next_day_docs.values():
						y_shift = [key for key, value in previous_day_docs.items() if value == doctor][0]
						t_shift = [key for key, value in next_day_docs.items() if value == doctor][0]
						if shift in shift_restrictions_y[y_shift] and shift in shift_restrictions_t[t_shift]:
							og_dict[shift].append(doctor)

	elif previous_day_schedule is not None and next_day_schedule is None:
		previous_day_docs = get_doctors_and_shifts(previous_day_schedule)
		for shift in available_shifts:
			shift_type = shift_types[shift]
			for doctor in available_doctors:
				if total_hours[doctor] + (doctor_shifts[shift][1] - doctor_shifts[shift][0]) >= max_hours[doctor] and shift not in ('Admin on call', 'Backup night', 'Backup day'): continue														#Check Total Hours
				if doctor_workload[doctor] + (doctor_shifts[shift][1] - doctor_shifts[shift][0]) >= max_limit[doctor] and shift not in ('Admin on call', 'Backup night', 'Backup day'): continue													#Check Doctor Workload
				if day_shifts[doctor] >= max_day_shifts[doctor] and shift_type == 'day': continue																				#Check Day Shifts
				if night_shifts[doctor] >= max_night_shifts[doctor] and shift_type == 'night': continue																			#Check Night Shifts
				if backup_shifts[doctor] >= max_backup_shifts[doctor] and shift_type == 'backup': continue																		#Check Backup Shifts
				if weekends[doctor] >= max_weekends[doctor] and current_weekday == 5 or weekends[doctor] >= max_weekends[doctor] and current_weekday == 6: continue				#Check Weekend Shifts

				restricted = False
				current_restrictions = unavailable_hours[doctor]
				for restriction in current_restrictions:
					for x in range(doctor_shifts[shift][0], doctor_shifts[shift][1] + 1):
						if restriction.Time == x and restriction.Weekday == current_weekday:
							restricted = True

				if restricted == True: 
					continue	

				elif doctor not in previous_day_docs.values() and doctor not in og_dict[shift]: og_dict[shift].append(doctor)
				elif doctor in previous_day_docs.values() and doctor not in og_dict[shift]:
					y_shift = [key for key, value in previous_day_docs.items() if value == doctor][0]
					if shift in shift_restrictions_y[y_shift]:
						og_dict[shift].append(doctor)

	elif previous_day_schedule is None and next_day_schedule is not None:
		next_day_docs = get_doctors_and_shifts(next_day_schedule)
		for shift in available_shifts:
			shift_type = shift_types[shift]
			for doctor in available_doctors:
				if total_hours[doctor] + (doctor_shifts[shift][1] - doctor_shifts[shift][0]) >= max_hours[doctor] and shift not in ('Admin on call', 'Backup night', 'Backup day'): continue														#Check Total Hours
				if doctor_workload[doctor] + (doctor_shifts[shift][1] - doctor_shifts[shift][0]) >= max_limit[doctor] and shift not in ('Admin on call', 'Backup night', 'Backup day'): continue													#Check Doctor Workload
				if day_shifts[doctor] >= max_day_shifts[doctor] and shift_type == 'day': continue																				#Check Day Shifts
				if night_shifts[doctor] >= max_night_shifts[doctor] and shift_type == 'night': continue																			#Check Night Shifts
				if backup_shifts[doctor] >= max_backup_shifts[doctor] and shift_type == 'backup': continue																		#Check Backup Shifts
				if weekends[doctor] >= max_weekends[doctor] and current_weekday == 5 or weekends[doctor] >= max_weekends[doctor] and current_weekday == 6: continue				#Check Weekend Shifts
				
				restricted = False
				current_restrictions = unavailable_hours[doctor]
				for restriction in current_restrictions:
					for x in range(doctor_shifts[shift][0], doctor_shifts[shift][1] + 1):
						if restriction.Time == x and restriction.Weekday == current_weekday:
							restricted = True

				if restricted == True: 
					continue	

				elif doctor not in og_dict[shift]:
					if doctor not in next_day_docs.values():
						og_dict[shift].append(doctor)
					elif doctor in next_day_docs.values() and doctor not in og_dict[shift]:
						t_shift = [key for key, value in next_day_docs.items() if value == doctor][0]
						if shift in shift_restrictions_t[t_shift]:
							og_dict[shift].append(doctor)
						
	else:
		for shift in available_shifts:
			shift_type = shift_types[shift]
			for doctor in available_doctors:
				if total_hours[doctor] + (doctor_shifts[shift][1] - doctor_shifts[shift][0]) >= max_hours[doctor] and shift not in ('Admin on call', 'Backup night', 'Backup day'): continue														#Check Total Hours
				if doctor_workload[doctor] + (doctor_shifts[shift][1] - doctor_shifts[shift][0]) >= max_limit[doctor] and shift not in ('Admin on call', 'Backup night', 'Backup day'): continue													#Check Doctor Workload
				if day_shifts[doctor] >= max_day_shifts[doctor] and shift_type == 'day': continue																				#Check Day Shifts
				if night_shifts[doctor] >= max_night_shifts[doctor] and shift_type == 'night': continue																			#Check Night Shifts
				if backup_shifts[doctor] >= max_backup_shifts[doctor] and shift_type == 'backup': continue																		#Check Backup Shifts
				if weekends[doctor] >= max_weekends[doctor] and current_weekday == 5 or weekends[doctor] >= max_weekends[doctor] and current_weekday == 6: continue				#Check Weekend Shifts
				
				restricted = False
				current_restrictions = unavailable_hours[doctor]
				for restriction in current_restrictions:
					for x in range(doctor_shifts[shift][0], doctor_shifts[shift][1] + 1):
						if restriction.Time == x and restriction.Weekday == current_weekday:
							restricted = True

				if restricted == True: 
					continue	
				
				elif doctor not in og_dict[shift]: og_dict[shift].append(doctor)

	return og_dict

def delete_used_doctors(shuffled_master_list, doctor):
	for list_ in shuffled_master_list:
		if doctor in list_:
			list_.remove(doctor)


def random_doctor_row_from_master_list(shuffled_master_list):
	random_doctor_row = []
	for index, list_ in enumerate(shuffled_master_list):
		doctor = list_[0]					# ERROR lies here!
		random_doctor_row.append(doctor)
		delete_used_doctors(shuffled_master_list, doctor)	
	return random_doctor_row	

def update_BB_restrictions(doctor, shift, date, backup_shifts, weekends, night_shifts, day_shifts, total_hours, doctor_shifts, shift_types):
	shiftType = shift_types[shift]
	if shiftType == 'backup': backup_shifts[doctor] += 1
	elif shiftType == 'day': day_shifts[doctor] += 1
	elif shiftType == 'night': night_shifts[doctor] += 1

	if shift not in ('Admin on call', 'Backup night', 'Backup day'): total_hours[doctor] += (doctor_shifts[shift][1] - doctor_shifts[shift][0])
	if datetime.datetime.strptime(date, "%Y-%m-%d").weekday() == 5 or datetime.datetime.strptime(date, "%Y-%m-%d").weekday() == 6: weekends[doctor] += 1

def update_all_restrictions(doctor, shift, date, backup_shifts, weekends, night_shifts, day_shifts, doctor_workload, total_hours, doctor_shifts, shift_types):
	"""Update shift count for each doctor"""

	shiftType = shift_types[shift]
	
	if shiftType == 'backup': backup_shifts[doctor] += 1 	# if backup shift, 	increment by one for doctor key	
	elif shiftType == 'day': day_shifts[doctor] += 1		# if day shift,		increment by one for doctor key
	elif shiftType == 'night': night_shifts[doctor] += 1	# if night shift, 	increment by one for doctor key

	if shift not in ('Admin on call', 'Backup night', 'Backup day'): 
		doctor_workload[doctor] += (doctor_shifts[shift][1] - doctor_shifts[shift][0])
		total_hours[doctor] += (doctor_shifts[shift][1] - doctor_shifts[shift][0])
	if datetime.datetime.strptime(date, "%Y-%m-%d").weekday() == 5 or datetime.datetime.strptime(date, "%Y-%m-%d").weekday() == 6: weekends[doctor] += 1


def create_basic_doctor_rows(date, og_dict, permanent_shifts_PPH, all_add_on_shifts, list_of_last_names, weekends, backup_shifts, night_shifts, day_shifts, shift_types, doctor_workload, total_hours, doctor_shifts, list_of_doctors_total_csv, model, model1, time_index):
	"""
	Optimization function.
	Shuffle doctor rows,
	then add shifts in taking into account of the restrictions
	"""
	#Month Vector
	month = date.split('-')[1]
	month_vector = [0] * 12
	month_vector[int(month) - 1] = 1

	master_list = [og_dict[x] for x in permanent_shifts_PPH]	# list of doctors available for permanent shifts

	counter = 0
	objective = False
	while objective == False:
		shuffled_master_list = [random.sample(x, len(x)) for x in master_list]

		# list of last names of doctors
		random_doctor_row = random_doctor_row_from_master_list(shuffled_master_list)		# IndexError // ERROR LIES HERE

		day_schedule = pd.DataFrame(index=range(24))
		day_schedule = fill_day_schedule(permanent_shifts_PPH, random_doctor_row, list_of_last_names, doctor_shifts, list_of_doctors_total_csv, day_schedule)
		pph_list, pi_list = vectorize_row_with_PIP(date, day_schedule, month_vector, model, model1, list_of_doctors_total_csv)

		Overall = calculate_overall_score_with_PIP(pph_list, pi_list)
		pph_sum = sum(pph_list)
		pi_sum = sum(pi_list)

		#Check and Increment Counter
		counter += 1
		print(date, counter)

		#if Overall.count('Red') >= 1 or Overall.count('Yellow') >= 3:
		if pph_sum < pi_sum:
			if counter > 50 and pph_sum > 0.9 * (pi_sum):
				for i, doc in enumerate(random_doctor_row):
					update_all_restrictions(doc, permanent_shifts_PPH[i], date, backup_shifts, weekends, night_shifts, day_shifts, doctor_workload, total_hours, doctor_shifts, shift_types)

				day_schedule.insert(loc=0, column='Time', value=time_index)
				day_schedule['PPH score'] = pph_list
				day_schedule['Overall score'] = Overall
				day_schedule.replace(np.nan, '', regex = True, inplace=True)

				objective = True

			elif counter > 75 and pph_sum > 0.8 * (pi_sum):
				for i, doc in enumerate(random_doctor_row):
					update_all_restrictions(doc, permanent_shifts_PPH[i], date, backup_shifts, weekends, night_shifts, day_shifts, doctor_workload, total_hours, doctor_shifts, shift_types)

				day_schedule.insert(loc=0, column='Time', value=time_index)
				day_schedule['PPH score'] = pph_list
				day_schedule['Overall score'] = Overall
				day_schedule.replace(np.nan, '', regex = True, inplace=True)

				objective = True

			elif counter > 100:
				print(Overall)

				day_schedule, Overall, random_doctor_row, pph_list, pi_list, expanded_shifts_PPH, pph_sum, pi_sum = add_extra_shifts(date, Overall, og_dict, month_vector, permanent_shifts_PPH, all_add_on_shifts, doctor_shifts, list_of_last_names, list_of_doctors_total_csv, model, model1)

				for i, doc in enumerate(random_doctor_row):
					update_all_restrictions(doc, expanded_shifts_PPH[i], date, backup_shifts, weekends, night_shifts, day_shifts, doctor_workload, total_hours, doctor_shifts, shift_types)
			
				day_schedule.insert(loc=0, column='Time', value=time_index)
				day_schedule['PPH score'] = pph_list
				day_schedule['Overall score'] = Overall
				day_schedule.replace(np.nan, '', regex = True, inplace=True)

				objective = True

			else: continue
			
		else: 
			#print(pph_list, Overall)
			#Update doctor workload
			for i, doc in enumerate(random_doctor_row):
				update_all_restrictions(doc, permanent_shifts_PPH[i], date, backup_shifts, weekends, night_shifts, day_shifts, doctor_workload, total_hours, doctor_shifts, shift_types)

			day_schedule.insert(loc=0, column='Time', value=time_index)
			day_schedule['PPH score'] = pph_list
			day_schedule['Overall score'] = Overall
			day_schedule.replace(np.nan, '', regex = True, inplace=True)

			objective = True

	return day_schedule, pph_sum, pi_sum, random_doctor_row

def add_backup_shifts(date, day_schedule, random_doctor_row, available_admin, list_of_doctors_total_csv, list_of_last_names, og_dict, doctor_shifts, backup_shifts, weekends, Restrictions):
	"""Function returns Pandas day schedule after adding backup shifts"""

	available_admin = [admin for admin in available_admin if backup_shifts[admin] < (Restrictions.query.filter_by(DoctorName=admin).first()).NumberBackupShifts and admin not in random_doctor_row]
	available_backup_night = [doctor for doctor in og_dict['Backup night'] if doctor not in random_doctor_row]
	available_backup_day = [doctor for doctor in og_dict['Backup day'] if doctor not in random_doctor_row]

	random_admin = random.choice(available_admin)
	if random_admin in available_backup_night: available_backup_night.remove(random_admin)
	if random_admin in available_backup_day: available_backup_day.remove(random_admin)
	random_backup_night = random.choice(available_backup_night)
	if random_backup_night in available_backup_day: available_backup_day.remove(random_backup_night)
	random_backup_day = random.choice(available_backup_day)

	admin_on_call = [list_of_doctors_total_csv[list_of_last_names.index(random_admin)] for i in range(0, doctor_shifts['Admin on call'][1] - doctor_shifts['Admin on call'][0])]
	backup_day = [''] * 24
	backup_night = [''] * 24

	for time in range(doctor_shifts['Backup day'][0] - 7, doctor_shifts['Backup day'][1] - 7):
		backup_day[time] = list_of_doctors_total_csv[list_of_last_names.index(random_backup_day)]

	for time in range(doctor_shifts['Backup night'][0] - 7, doctor_shifts['Backup night'][1] - 7):
		backup_night[time] = list_of_doctors_total_csv[list_of_last_names.index(random_backup_night)]

	backup_shifts[random_admin] += 1
	backup_shifts[random_backup_day] += 1
	backup_shifts[random_backup_night] += 1

	if datetime.datetime.strptime(date, "%Y-%m-%d").weekday() == 5 or datetime.datetime.strptime(date, "%Y-%m-%d").weekday() == 6:
		weekends[random_admin] += 1
		weekends[random_backup_day] += 1
		weekends[random_backup_night] += 1

	day_schedule.insert(loc=1, column='Admin on call', value=admin_on_call)
	day_schedule.insert(loc=2, column='Backup day', value=backup_day)
	day_schedule.insert(loc=3, column='Backup night', value=backup_night)

	return day_schedule

def unique(sequence):
	"""Return list of unique sequence"""
	seen = set()
	return [x for x in sequence if not (x in seen or seen.add(x))]

def find_add_on_shifts(red_hours, doctor_shifts, check):
	"""Check if the time corresponds to the overall red"""
	add_ons = []
	for hour in red_hours:
		for shift in check:
			if doctor_shifts[shift][0] < hour < doctor_shifts[shift][1]:
				add_ons.append(shift)

	add_ons = [item for items, c in collections.Counter(add_ons).most_common() for item in [items] * c]			
	return add_ons

def schedule_df_to_html(day_schedule):
	"""Convert day schedule to HTML table with color coding and various formatting"""
	html_table = day_schedule.to_html(index=False)
	original_string = """<table border="1" class="dataframe">"""
	new_string = """<table style="width:auto; table-layout:fixed;" class="table table-bordered">"""


	# format HTML table content
	html_table = html_table.replace(original_string, new_string)
	html_table = html_table.replace("""!""", """<p>""")
	html_table = html_table.replace("""!!""", """</p>""")
	html_table = html_table.replace("""?""", """<p class=yellow>""")
	html_table = html_table.replace("""??""", """</p>""")

	# replacing keywords with HEX colors
	html_table = html_table.replace("""<th>Time</th>""", """<th width=90>Time</th>""")
	html_table = html_table.replace("""<td>Green</td>""", """<td bgcolor="#19D529"></td>""")
	html_table = html_table.replace("""<td>Yellow</td>""", """<td bgcolor="#FFD400"></td>""")
	html_table = html_table.replace("""<td>Red</td>""", """<td bgcolor="#FF4900"></td>""")	
	html_table = html_table.replace("""<td>Orange</td>""", """<td bgcolor="#FFC300"></td>""")
	html_table = html_table.replace("""<td>Blue</td>""", """<td bgcolor="#4F95FF"></td>""")

	return html_table

def return_list_of_indices_ascending_order_pph_minus_pi(pph_minus_pi_list):
	"""Function returns list of indices of days based on their (PPH - PI) values in ascending order"""
	copy_of_pph_minus_pi_list = pph_minus_pi_list.copy()
	list_ascending_order = []
	while len(copy_of_pph_minus_pi_list) > 0:
		min_of_copy_of_pph_minus_pi_list = min(copy_of_pph_minus_pi_list)
		worst_days_pph_minus_pi_index = [index for index, value in enumerate(pph_minus_pi_list) if value == min_of_copy_of_pph_minus_pi_list]
		for index in worst_days_pph_minus_pi_index:
			list_ascending_order.append(index)
		while min_of_copy_of_pph_minus_pi_list in copy_of_pph_minus_pi_list:
			copy_of_pph_minus_pi_list.remove(min_of_copy_of_pph_minus_pi_list)
		# copy_of_pph_minus_pi_list[:] = [x for x in copy_of_pph_minus_pi_list if x!=min_of_copy_of_pph_minus_pi_list]
	return list_ascending_order

def add_extra_shifts(date, Overall, og_dict, month_vector, permanent_shifts_PPH, all_add_on_shifts, doctor_shifts, list_of_last_names, list_of_doctors_total_csv, model, model1):
	"""Adds extra shifts in taking into account of the restrictions"""
	red_hours = [(i + 7) for i, x in enumerate(Overall) if x == "Red" or x == "Yellow"]
	add_on_shifts = find_add_on_shifts(red_hours, doctor_shifts, all_add_on_shifts)
	unique_add_ons = unique(add_on_shifts)
	expanded_shifts_PPH = permanent_shifts_PPH.copy()
	add_on_counter = 0

	for helper in unique_add_ons:
		add_on_counter += 1
		expanded_shifts_PPH.append(helper)
		trial_error = 0
		master_list = [og_dict[x] for x in expanded_shifts_PPH]

		for x in range(0, (75 * add_on_counter)):
			shuffled_master_list = [random.sample(x, len(x)) for x in master_list]
			random_doctor_row = random_doctor_row_from_master_list(shuffled_master_list)
			day_schedule = pd.DataFrame(index=range(24))
			day_schedule = fill_day_schedule(expanded_shifts_PPH, random_doctor_row, list_of_last_names, doctor_shifts, list_of_doctors_total_csv, day_schedule)
			pph_list, pi_list = vectorize_row_with_PIP(date, day_schedule, month_vector, model, model1, list_of_doctors_total_csv)

			Overall = calculate_overall_score_with_PIP(pph_list, pi_list)
			pph_sum = sum(pph_list)
			pi_sum = sum(pi_list)

			trial_error += 1
			#if Overall.count('Red') >= 1 or Overall.count('Yellow') >= 3:
			if pph_sum < pi_sum:
				if trial_error > (25 * add_on_counter) and pph_sum > 0.9 * pi_sum:
					return day_schedule, Overall, random_doctor_row, pph_list, pi_list, expanded_shifts_PPH, pph_sum, pi_sum
				elif trial_error > (50 * add_on_counter) and pph_sum > 0.8 * pi_sum:
					return day_schedule, Overall, random_doctor_row, pph_list, pi_list, expanded_shifts_PPH, pph_sum, pi_sum
				else: continue
			else:
				return day_schedule, Overall, random_doctor_row, pph_list, pi_list, expanded_shifts_PPH, pph_sum, pi_sum

	return "No solution found!"

'''
def add_BB_shifts(date, Overall, og_dict, month_vector, permanent_shifts_PPH, all_add_on_shifts, doctor_shifts, list_of_last_names, list_of_doctors_total_csv, model, model1, shift_tracker):
	red_hours = [(i + 7) for i, x in enumerate(Overall) if x == "Red" or x == "Yellow"]
	add_on_shifts = find_add_on_shifts(red_hours, doctor_shifts, all_add_on_shifts)
	unique_add_ons = unique(add_on_shifts)
	expanded_shifts = permanent_shifts_PPH.copy()
	add_on_counter = 0

	for helper in unique_add_ons:
		if shift_tracker == 0:
			return

		add_on_counter += 1
		shift_tracker -= 1
		expanded_shifts.append(helper)
		trial_error = 0
		master_list = [og_dict[x] for x in expanded_shifts]

		for x in range(0, (100 * add_on_counter)):
			shuffled_master_list = [random.sample(x, len(x)) for x in master_list]
			random_doctor_row = random_doctor_row_from_master_list(shuffled_master_list)
			day_schedule = pd.DataFrame(index=range(24))
			day_schedule = fill_day_schedule(expanded_shifts, random_doctor_row, list_of_last_names, doctor_shifts, list_of_doctors_total_csv, day_schedule)
			pph_list, pi_list = vectorize_row_with_PIP(date, day_schedule, month_vector, model, model1, list_of_doctors_total_csv)

			Overall = calculate_overall_score_with_PIP(pph_list, pi_list)
			pph_sum = sum(pph_list)
			pi_sum = sum(pi_list)

			trial_error += 1

'''

def remove_BB_shifts(date, og_dict, permanent_shifts_PPH, add_on_shifts, list_of_last_names, doctor_shifts, list_of_doctors_total_csv, month_vector, model, model1, shift_tracker):
	"""Removes extra shifts until overstaffing is eradicated"""
	extra_shifts = [shift for shift in permanent_shifts_PPH if shift in add_on_shifts]

	if extra_shifts is None: raise ValueError

	diminished_shifts = permanent_shifts_PPH.copy()
	removal_counter = 0

	for extra in extra_shifts:
		removal_counter += 1
		diminished_shifts.remove(extra)
		trial_error = 0
		master_list = [og_dict[x] for x in diminished_shifts]

		for x in range(0, (100 * removal_counter)):
			shuffled_master_list = [random.sample(x, len(x)) for x in master_list]
			random_doctor_row = random_doctor_row_from_master_list(shuffled_master_list)
			day_schedule = pd.DataFrame(index=range(24))
			day_schedule = fill_day_schedule(diminished_shifts, random_doctor_row, list_of_last_names, doctor_shifts, list_of_doctors_total_csv, day_schedule)
			pph_list, pi_list = vectorize_row_with_PIP(date, day_schedule, month_vector, model, model1, list_of_doctors_total_csv)

			Overall = calculate_overall_score_with_PIP(pph_list, pi_list)
			pph_sum = sum(pph_list)
			pi_sum = sum(pi_list)

			trial_error += 1

			if (1.10 * pi_sum) >= pph_sum >= (0.85 * pi_sum):
				shift_tracker += removal_counter
				return day_schedule, Overall, random_doctor_row, pph_list, pi_list, diminished_shifts, pph_sum, pi_sum
			elif pph_sum > (1.10 * pi_sum):
				continue
			elif pph_sum < (0.85 * pi_sum):
				continue
	return "Can't effectively remove shifts"


def optimize_BB_schedule(date, og_dict, permanent_shifts_PPH, list_of_last_names, backup_shifts, weekends, night_shifts, day_shifts, shift_types, doctor_workload, doctor_shifts, list_of_doctors_total_csv, model, model1, time_index):
	"""Optimize ByteBloc schedule by swapping overstaffed shifts with understaffed ones
	Returns pandas dataframe of that day's schedule PPH sum and PI sum"""

	#Month Vector
	month = date.split('-')[1]
	month_vector = [0] * 12
	month_vector[int(month) - 1] = 1

	master_list = [og_dict[x] for x in permanent_shifts_PPH]

	counter = 0
	objective = False
	while objective == False:
		shuffled_master_list = [random.sample(x, len(x)) for x in master_list]
		random_doctor_row = random_doctor_row_from_master_list(shuffled_master_list)
		day_schedule = pd.DataFrame(index=range(24))
		day_schedule = fill_day_schedule(permanent_shifts_PPH, random_doctor_row, list_of_last_names, doctor_shifts, list_of_doctors_total_csv, day_schedule)
		pph_list, pi_list = vectorize_row_with_PIP(date, day_schedule, month_vector, model, model1, list_of_doctors_total_csv)

		Overall = calculate_overall_score_with_PIP(pph_list, pi_list)
		pph_sum = sum(pph_list)
		pi_sum = sum(pi_list)

		#Check and Increment Counter
		counter += 1
		print(date, counter)

		if pph_sum > (1.10 * pi_sum):
			continue

		elif pph_sum < (0.9 * pi_sum):
			continue

		elif  (1.10 * pi_sum) >= pph_sum >= (0.85 * pi_sum):
			for i, doc in enumerate(random_doctor_row):
				update_all_restrictions(doc, permanent_shifts_PPH[i], date, backup_shifts, weekends, night_shifts, day_shifts, doctor_workload, doctor_shifts, shift_types)

			day_schedule.insert(loc=0, column='Time', value=time_index)
			day_schedule['PPH score'] = pph_list
			day_schedule['Overall score'] = Overall
			day_schedule.replace(np.nan, '', regex = True, inplace=True)

			objective = True

		elif counter > 10 and pph_sum > (1.10 * pi_sum):
			print("REMOVING SHIFT")
			day_schedule, Overall, random_doctor_row, pph_list, pi_list, diminished_shifts, pph_sum, pi_sum = remove_BB_shifts(date, og_dict, permanent_shifts_PPH, all_add_on_shifts, list_of_last_names, doctor_shifts, list_of_doctors_total_csv, month_vector, model, model1)

			for i, doc in enumerate(random_doctor_row):
				update_all_restrictions(doc, diminished_shifts[i], date, backup_shifts, weekends, night_shifts, day_shifts, doctor_workload, total_hours, doctor_shifts, shift_types)

			day_schedule.insert(loc=0, column='Time', value=time_index)
			day_schedule['PPH score'] = pph_list
			day_schedule['Overall score'] = Overall
			day_schedule.replace(np.nan, '', regex=True, inplace=True)

			objective = True

		elif counter > 100 and pph_sum < (0.85 * pi_sum):
			#Add Shift counting variable
			print('ADDING SHIFT')
			day_schedule, Overall, random_doctor_row, pph_list, pi_list, expanded_shifts_PPH, pph_sum, pi_sum = add_extra_shifts(date, Overall, og_dict, month_vector, permanent_shifts_PPH, all_add_on_shifts, doctor_shifts, list_of_last_names, list_of_doctors_total_csv, model, model1)

			for i, doc in enumerate(random_doctor_row):
				update_all_restrictions(doc, expanded_shifts_PPH[i], date, backup_shifts, weekends, night_shifts, day_shifts, doctor_workload, total_hours, doctor_shifts, shift_types)
		
			day_schedule.insert(loc=0, column='Time', value=time_index)
			day_schedule['PPH score'] = pph_list
			day_schedule['Overall score'] = Overall
			day_schedule.replace(np.nan, '', regex = True, inplace=True)

			objective = True

		else: 
			for i, doc in enumerate(random_doctor_row):
				update_all_restrictions(doc, permanent_shifts_PPH[i], date, backup_shifts, weekends, night_shifts, day_shifts, doctor_workload, doctor_shifts, shift_types)

			day_schedule.insert(loc=0, column='Time', value=time_index)
			day_schedule['PPH score'] = pph_list
			day_schedule['Overall score'] = Overall
			day_schedule.replace(np.nan, '', regex = True, inplace=True)

			objective = True

	return day_schedule, pph_sum, pi_sum